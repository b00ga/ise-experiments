`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    20:45:40 03/15/2015 
// Design Name: 
// Module Name:    MINUTES 
// Project Name: Kitchen Timer
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MINUTES(
		output reg [3:0] tensdigit = 4'b0,
		output reg [3:0] onesdigit = 4'b0,
		output reg timesup = 1'b0,
		input wire run,
		input wire en,
		input wire minutecarry,
		input wire clock
    );
	 
	reg [25:0] timer = 26'b0;	 
	 
	always @(posedge clock)
	begin
		if (!run)
		begin
			if (en)
				if ( onesdigit == 9 )
					onesdigit <= 4'b0;
				else
					onesdigit <= onesdigit + 1;
		end
		else if (timer==26'd49999999)
		begin
			if ( minutecarry )
				if ( onesdigit == 0 )
					onesdigit <= 4'd9;
				else
					onesdigit <= onesdigit - 1;
		end
	end

	always @(posedge clock)
	begin
		if (!run)
		begin
			if (en)
				if ( (tensdigit == 9) && (onesdigit == 9) )
					tensdigit <= 4'b0;
				else if ( onesdigit == 9 )
					tensdigit <= tensdigit +1;
		end
		else if (timer==26'd49999999)
		begin
			if ( minutecarry && (onesdigit == 0) )
				tensdigit <= tensdigit - 1;
		end
	end
	
	always @(posedge clock)
	begin
		if ( (tensdigit == 0) && (onesdigit == 0) && run )
			timesup <= 1'b1;
		else
			timesup <= 1'b0;
	end

	always @(posedge clock)
	begin
		if (run)
		begin
			if ( timer < 50000000 )
				timer <= timer + 1;
			else
				timer <= 26'b0;
		end
		else
			timer <= 26'b0;
	end
endmodule
