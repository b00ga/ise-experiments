`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    14:57:29 02/25/2015 
// Design Name: 
// Module Name:    TOP 
// Project Name: Kitchen Timer
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TOP(
		output wire [3:0] digitselect,
		output wire [7:0] segmentselect,
		output wire runstate,
		output wire blink,
		input wire [2:0] btn,
		input wire clock	
    );

wire [2:0] sync;
//wire [3:0] num;
wire[3:0] secsones;
wire [3:0] secstens;
wire[3:0] minsones;
wire [3:0] minstens;
wire minutecarry;
wire minsup;
wire secsup;

DBNC button0(sync[0],btn[0],clock);
DBNC button1(sync[1],btn[1],clock);
DBNC button2(sync[2],btn[2],clock);
//ADDER a1(num,sync[1],clock);
SECONDS s1(secstens,secsones,secsup,minutecarry,runstate,sync[1],clock);
MINUTES m1(minstens,minsones,minsup,runstate,sync[2],minutecarry,clock);
C4HEXDISP c4hex(segmentselect,digitselect,minstens,minsones,secstens,secsones,clock);
//C4HEXDISP c4hex(segmentselect,digitselect,4'b0,4'b0,secstens,secsones,clock);
RUNSTATE rs(runstate,sync[0],minsup & secsup,clock);
BLINK b(blink,minsup & secsup,clock);

endmodule
