`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    20:58:40 03/15/2015 
// Design Name: 
// Module Name:    RUNSTATE 
// Project Name: Kitchen Timer
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module RUNSTATE(
		output reg state = 1'b0,
		input	wire en,
		input wire reset,
		input wire clock
    );

	always @(posedge clock)
		if (en)
			state <= ~state;
		else if (reset)
			state <= 1'b0;

endmodule
