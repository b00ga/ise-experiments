`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    15:14:31 02/25/2015 
// Design Name: 
// Module Name:    COUNTER 
// Project Name: Kitchen Timer
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module COUNTER(
                output reg [1:0] sum = 2'b0,
                input wire en,
                input wire clock
    );

        always @(posedge clock)
        begin
				if (en)
					sum <= sum + 1;
        end
endmodule

