`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    15:16:40 02/25/2015 
// Design Name: 
// Module Name:    SELECTENCODER 
// Project Name: Kitchen Timer
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SELECTENCODER(
		output reg [3:0] digitselect,
		input wire [1:0] num
    );

	always @(num)
	begin
		case (num)
			2'b00:	 digitselect <= 4'b1110 ;
			2'b01:	 digitselect <= 4'b1101 ;
			2'b10:	 digitselect <= 4'b1011 ;
			2'b11:	 digitselect <= 4'b0111 ;
		endcase
	end

endmodule
