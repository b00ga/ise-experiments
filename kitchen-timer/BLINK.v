`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    21:48:44 03/17/2015 
// Design Name: 
// Module Name:    BLINK 
// Project Name: Kitchen Timer
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BLINK(
		output reg blink,
		input en,
		input clock
    );
	 
	reg [25:0] timer = 26'b0;
	reg [1:0] numblinks = 2'd3;

	always @(posedge clock)
	begin
		if (timer==26'd24999999)
			blink <= 1'b0;
		else if (timer==26'd49999999)
			if ( numblinks < 3 )
				blink <= 1'b1;
	end
	
	always @(posedge clock)
	begin
		if (en)
			numblinks <= 2'b0;
		else if ( timer==26'd49999999 )
			if ( numblinks < 3 )
				numblinks <= numblinks + 1;
	end

	always @(posedge clock)
	begin
		if ( timer < 50000000 )
			timer <= timer + 1;
		else
			timer <= 26'b0;
	end		

endmodule
