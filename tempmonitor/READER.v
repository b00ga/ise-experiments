`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    18:38:53 04/11/2015 
// Design Name: 
// Module Name:    READER 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description: Fetch parallel output 8-bit value from ADC
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module READER(
    output reg [7:0] ADCOUT = 8'b0,
	 output reg CS = 1,
	 output reg RD = 1,
    input wire [7:0] DB,
    input wire EN,
    input wire CLK
    );

	reg [7:0] DATA =0;
	reg WAIT = 0;

	always @(posedge CLK)
	begin
		if (EN)
			WAIT <= 1;
		else
			WAIT <= 0;
	end
	
	always @(posedge CLK)
	begin
		if (EN)
			CS <= 0;
		else
			CS <= 1;
	end

	always @(posedge CLK)
	begin
		if (EN)
			RD <= 0;
		else
			RD <= 1;
	end
	
	always @(posedge CLK)
	begin
		if (WAIT && !EN)
			DATA = DB;
	end

	always @(DATA)
	begin
		ADCOUT <= DATA;
	end
	
endmodule
