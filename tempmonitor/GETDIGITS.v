`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    16:51:33 04/12/2015 
// Design Name: 
// Module Name:    GETDIGITS 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description:  Use divider core to get individual digits for display
//		UNUSED IN FINAL DESIGN
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module GETDIGITS(
    output reg [3:0] hundredsdigit,
    output wire [3:0] tensdigit,
    output wire [3:0] onesdigit,
    input wire [7:0] num,
    input wire CLK
    );

	wire rfd1, rfd2;
	wire [7:0] quotient1, quotient2;
	
	always @(quotient2)
	begin
		hundredsdigit <= quotient2[3:0];
	end
	
	DIV10 div1 (
	.clk(CLK), // input clk
	.rfd(rfd1), // output rfd
	.dividend(num), // input [7 : 0] dividend
	.divisor(4'd10), // input [3 : 0] divisor
	.quotient(quotient1), // output [7 : 0] quotient
	.fractional(onesdigit)); // output [3 : 0] fractional
	
	DIV10 div2 (
	.clk(CLK), // input clk
	.rfd(rfd2), // output rfd
	.dividend(quotient1), // input [7 : 0] dividend
	.divisor(4'd10), // input [3 : 0] divisor
	.quotient(quotient2), // output [7 : 0] quotient
	.fractional(tensdigit)); // output [3 : 0] fractional

endmodule
