`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    15:07:17 02/25/2015 
// Design Name: 
// Module Name:    TICKER 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description:  Output an enable pulse every 4ms for digit refresh rate on 7seg
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module C4HEXTICKER(
        output reg tick = 1'b0,
        input wire clock
    );

		reg [17:0] ticker = 18'd0;

		always @(posedge clock)
				if (ticker == 18'd199999)
					ticker <= 0;
				else if (ticker == 18'd0)
					ticker <= 18'd1;
				else
					ticker <= ticker + 1;
					
		always @(posedge clock)
			if (ticker == 18'd199999)
				tick <= 1'b1;
			else if (ticker == 18'd0)
				tick <= 1'b0;

endmodule

