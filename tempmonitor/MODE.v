`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    12:54:16 04/20/2015 
// Design Name: 
// Module Name:    MODE 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description: Determine what mode we are in (display, set warn, set alaram)
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module MODE(
		output reg [1:0] mode = 2'b0,
		input wire EN,
		input clock
    );

	always @(posedge clock)
	begin
		if (EN)
		begin
			if ( mode == 2'b10 )
				mode <= 2'b0;
			else
				mode <= mode + 1;
		end
	end

endmodule
