`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    14:59:45 02/16/2015 
// Design Name: 
// Module Name:    TICKER 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description:  Pulse evert 1s (every 50M clock pulses)
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TICKER(
	output reg tick = 1'b0,
	input wire clock
    );

	reg [25:0] ticker = 25'd0;
	
	always @(posedge clock)
	begin
		if (ticker == 26'd49999999)
			ticker <= 0;
		else
			ticker <= ticker + 1;
	end

	always @(posedge clock)
	begin
		if (ticker == 26'd49999999)
			tick <= 1'b1;
		else
			tick <= 1'b0;
	end

endmodule
