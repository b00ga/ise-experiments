`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    18:29:29 04/11/2015 
// Design Name: 
// Module Name:    CONVWAIT 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description: ADC7819 takes max 6us to convert. This module provides 
//		that holdoff time
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CONVWAIT(
    output reg DONE,
    input wire EN,
    input wire CLK
    );

	reg [8:0] ticker = 0;
	reg counting = 0;
	
	always @(posedge CLK)
	begin
		if (EN)
			ticker <= 0;
		else
			ticker <= ticker+ 1;
	end
	
	always @(posedge CLK)
	begin
		if (EN)
			counting <= 1;
		else if (counting && ticker==299)
			counting <= 0;
	end
	
	always @(posedge CLK)
	begin
		if (counting && ticker==299)
			DONE <= 1;
		else
			DONE <= 0;
	end

endmodule
