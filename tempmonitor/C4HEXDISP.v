`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    14:58:31 02/25/2015 
// Design Name: 
// Module Name:    C4HEXDISP 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description: Takes in 4 4-bit numvers and outputs them to a 4-digit
//		7-segment display one digit at a time at a proper referesh rate
//
// Dependencies:  C4HEXTICKER, COUTNER, SELECTENCODER, SEGENCODER
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module C4HEXDISP(
		output wire [7:0] SEG_N,
		output wire [3:0] EN_N,
		input wire [3:0] C3,
		input wire [3:0] C2,
		input wire [3:0] C1,
		input wire [3:0] C0,
		input wire clock
    );

wire tick;
wire [1:0] count;
reg [3:0] currentdigit;

C4HEXTICKER cht1(tick,clock);
COUNTER c1(count, tick, clock);
SELECTENCODER en1(EN_N,count);
SEGENCODER en2(SEG_N,currentdigit);

always @(EN_N)
begin
	case (count)
		4'b00:	currentdigit <= C0;
		4'b01:	currentdigit <= C1;
		4'b10:	currentdigit <= C2;
		4'b11:	currentdigit <= C3;
	endcase
end

endmodule
