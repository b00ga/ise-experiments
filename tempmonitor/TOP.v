`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    14:27:13 04/11/2015 
// Design Name: 
// Module Name:    TOP 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description: TOP level module for temperature monitor
//
// Dependencies: DBNC, TICKER, CONVST, CONVWAIT, READER, LOOKUP, C4HEXDISP,
//			STOPLIGHT, MODE, THRESHOLD
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TOP(
		output wire [3:0] digitselect,
		output wire [7:0] segmentselect,
		output wire convst,
		output wire cs,
		output wire rd,
		output reg[7:0] led,
		output wire[2:0] stoplight,
		input wire[7:0] adcdb,
		input wire[1:0] btn,
		input wire clock
    );
	 
	wire tick;
	wire convdone;
	wire [3:0] hundredsdigit, tensdigit, onesdigit;
	reg [7:0] tempF = 8'b0, tempC = 8'b0;
	wire [7:0] adcreading;
	wire [1:0] sync;
	
	wire [1:0] mode;
	wire [3:0] warnten, warnone;
	wire [3:0] alarmten, alarmone;
	
	reg [3:0] digit1 = 4'b0;
	reg [3:0] digit2 = 4'b0;
	reg [3:0] digit3 = 4'b0;
	reg [3:0] digit4 = 4'b0;

	DBNC button0(sync[0],btn[0],clock);
	DBNC button1(sync[1],btn[1],clock);
 
	TICKER t1(tick,clock);
	CONVST start(convst,tick,clock);
	CONVWAIT cwait(convdone,tick,clock);
	READER r1(adcreading,cs,rd,adcdb,convdone,clock);
	LOOKUP l1(hundredsdigit,tensdigit,onesdigit,led);
	C4HEXDISP c4hex(segmentselect,digitselect,digit1,digit2,digit3,digit4,clock);
	STOPLIGHT sl(stoplight,hundredsdigit,tensdigit,onesdigit,warnten*10+warnone,alarmten*10+alarmone,clock);
	MODE m1(mode,sync[1],clock);
	THRESHOLD th1(warnten,warnone,alarmten,alarmone,mode,sync[0],clock);

	always @(adcreading)
	begin
		led <= adcreading;
	end

	always @(posedge clock)
	begin
		if ( mode == 0 )
			digit1 <= hundredsdigit;
		else if ( mode == 1 )
			digit1 <= 4'hA;
		else
			digit1 <= 4'hB;
	end
	always @(posedge clock)
	begin
		if ( mode == 0 )
			digit2 <= tensdigit;
		else if ( mode == 1 )
			digit2 <= warnten;
		else
			digit2 <= alarmten;
	end
	always @(posedge clock)
	begin
		if ( mode == 0 )
			digit3 <= onesdigit;
		else if ( mode == 1 )
			digit3 <= warnone;
		else
			digit3 <= alarmone;
	end
	always @(posedge clock)
	begin
		digit4 <= 4'hF;
	end


endmodule
