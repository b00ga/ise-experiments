`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    13:32:00 04/20/2015 
// Design Name: 
// Module Name:    THRESHOLD 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description:  Allows settings of warning and alarm thresholds
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module THRESHOLD(
		output reg [3:0] warnten = 4'd7,
		output reg [3:0] warnone = 4'd0,
		output reg [3:0] alarmten= 4'd7,
		output reg [3:0] alarmone= 4'd3,
		input wire [1:0] mode,
		input wire EN,
		input wire clock
    );

	reg warncarry=1'b0, alarmcarry=1'b0;

	always @(posedge clock)
	begin
		if (EN && (mode==2'b01) )
			if (warncarry==1)
				if (warnten == 9)
					warnten <= 6;
				else
					warnten <= warnten+1;
	end
	always @(posedge clock)
	begin
		if (EN && (mode==2'b01) )
			if (warnone==9)
				warnone <= 4'd0;
			else
				warnone <= warnone +1;
	end
	always @(posedge clock)
	begin
		if (EN && (mode==2'b01) )
			if (warnone==8)
				warncarry <= 1'b1;
			else
				warncarry <= 1'b0;
	end		
	always @(posedge clock)
	begin
		if (EN && (mode==2'b10) )
			if (alarmcarry==1)
				if (alarmten == 9)
					alarmten <= 6;
				else
					alarmten <= alarmten+1;
	end
	always @(posedge clock)
	begin
		if (EN && (mode==2'b10) )
			if (alarmone==9)
				alarmone <= 4'd0;
			else
				alarmone <= alarmone +1;
	end
	always @(posedge clock)
	begin
		if (EN && (mode==2'b10) )
			if (alarmone==8)
				alarmcarry <= 1'b1;
			else
				alarmcarry <= 1'b0;
	end

endmodule
