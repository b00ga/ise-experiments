////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: DIV10.v
// /___/   /\     Timestamp: Sun Apr 12 16:54:27 2015
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -w -sim -ofmt verilog ./tmp/_cg/DIV10.ngc ./tmp/_cg/DIV10.v 
// Device	: 3s100ecp132-5
// Input file	: ./tmp/_cg/DIV10.ngc
// Output file	: ./tmp/_cg/DIV10.v
// # of Modules	: 1
// Design Name	: DIV10
// Xilinx        : /opt/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module DIV10 (
  rfd, clk, dividend, quotient, divisor, fractional
)/* synthesis syn_black_box syn_noprune=1 */;
  output rfd;
  input clk;
  input [7 : 0] dividend;
  output [7 : 0] quotient;
  input [3 : 0] divisor;
  output [3 : 0] fractional;
  
  // synthesis translate_off
  
  wire NlwRenamedSig_OI_rfd;
  wire \blk00000003/sig0000014a ;
  wire \blk00000003/sig00000149 ;
  wire \blk00000003/sig00000148 ;
  wire \blk00000003/sig00000147 ;
  wire \blk00000003/sig00000146 ;
  wire \blk00000003/sig00000145 ;
  wire \blk00000003/sig00000144 ;
  wire \blk00000003/sig00000143 ;
  wire \blk00000003/sig00000142 ;
  wire \blk00000003/sig00000141 ;
  wire \blk00000003/sig00000140 ;
  wire \blk00000003/sig0000013f ;
  wire \blk00000003/sig0000013e ;
  wire \blk00000003/sig0000013d ;
  wire \blk00000003/sig0000013c ;
  wire \blk00000003/sig0000013b ;
  wire \blk00000003/sig0000013a ;
  wire \blk00000003/sig00000139 ;
  wire \blk00000003/sig00000138 ;
  wire \blk00000003/sig00000137 ;
  wire \blk00000003/sig00000136 ;
  wire \blk00000003/sig00000135 ;
  wire \blk00000003/sig00000134 ;
  wire \blk00000003/sig00000133 ;
  wire \blk00000003/sig00000132 ;
  wire \blk00000003/sig00000131 ;
  wire \blk00000003/sig00000130 ;
  wire \blk00000003/sig0000012f ;
  wire \blk00000003/sig0000012e ;
  wire \blk00000003/sig0000012d ;
  wire \blk00000003/sig0000012c ;
  wire \blk00000003/sig0000012b ;
  wire \blk00000003/sig0000012a ;
  wire \blk00000003/sig00000129 ;
  wire \blk00000003/sig00000128 ;
  wire \blk00000003/sig00000127 ;
  wire \blk00000003/sig00000126 ;
  wire \blk00000003/sig00000125 ;
  wire \blk00000003/sig00000124 ;
  wire \blk00000003/sig00000123 ;
  wire \blk00000003/sig00000122 ;
  wire \blk00000003/sig00000121 ;
  wire \blk00000003/sig00000120 ;
  wire \blk00000003/sig0000011f ;
  wire \blk00000003/sig0000011e ;
  wire \blk00000003/sig0000011d ;
  wire \blk00000003/sig0000011c ;
  wire \blk00000003/sig0000011b ;
  wire \blk00000003/sig0000011a ;
  wire \blk00000003/sig00000119 ;
  wire \blk00000003/sig00000118 ;
  wire \blk00000003/sig00000117 ;
  wire \blk00000003/sig00000116 ;
  wire \blk00000003/sig00000115 ;
  wire \blk00000003/sig00000114 ;
  wire \blk00000003/sig00000113 ;
  wire \blk00000003/sig00000112 ;
  wire \blk00000003/sig00000111 ;
  wire \blk00000003/sig00000110 ;
  wire \blk00000003/sig0000010f ;
  wire \blk00000003/sig0000010e ;
  wire \blk00000003/sig0000010d ;
  wire \blk00000003/sig0000010c ;
  wire \blk00000003/sig0000010b ;
  wire \blk00000003/sig0000010a ;
  wire \blk00000003/sig00000109 ;
  wire \blk00000003/sig00000108 ;
  wire \blk00000003/sig00000107 ;
  wire \blk00000003/sig00000106 ;
  wire \blk00000003/sig00000105 ;
  wire \blk00000003/sig00000104 ;
  wire \blk00000003/sig00000103 ;
  wire \blk00000003/sig00000102 ;
  wire \blk00000003/sig00000101 ;
  wire \blk00000003/sig00000100 ;
  wire \blk00000003/sig000000ff ;
  wire \blk00000003/sig000000fe ;
  wire \blk00000003/sig000000fd ;
  wire \blk00000003/sig000000fc ;
  wire \blk00000003/sig000000fb ;
  wire \blk00000003/sig000000fa ;
  wire \blk00000003/sig000000f9 ;
  wire \blk00000003/sig000000f8 ;
  wire \blk00000003/sig000000f7 ;
  wire \blk00000003/sig000000f6 ;
  wire \blk00000003/sig000000f5 ;
  wire \blk00000003/sig000000f4 ;
  wire \blk00000003/sig000000f3 ;
  wire \blk00000003/sig000000f2 ;
  wire \blk00000003/sig000000f1 ;
  wire \blk00000003/sig000000f0 ;
  wire \blk00000003/sig000000ef ;
  wire \blk00000003/sig000000ee ;
  wire \blk00000003/sig000000ed ;
  wire \blk00000003/sig000000ec ;
  wire \blk00000003/sig000000eb ;
  wire \blk00000003/sig000000ea ;
  wire \blk00000003/sig000000e9 ;
  wire \blk00000003/sig000000e8 ;
  wire \blk00000003/sig000000e7 ;
  wire \blk00000003/sig000000e6 ;
  wire \blk00000003/sig000000e5 ;
  wire \blk00000003/sig000000e4 ;
  wire \blk00000003/sig000000e3 ;
  wire \blk00000003/sig000000e2 ;
  wire \blk00000003/sig000000e1 ;
  wire \blk00000003/sig000000e0 ;
  wire \blk00000003/sig000000df ;
  wire \blk00000003/sig000000de ;
  wire \blk00000003/sig000000dd ;
  wire \blk00000003/sig000000dc ;
  wire \blk00000003/sig000000db ;
  wire \blk00000003/sig000000da ;
  wire \blk00000003/sig000000d9 ;
  wire \blk00000003/sig000000d8 ;
  wire \blk00000003/sig000000d7 ;
  wire \blk00000003/sig000000d6 ;
  wire \blk00000003/sig000000d5 ;
  wire \blk00000003/sig000000d4 ;
  wire \blk00000003/sig000000d3 ;
  wire \blk00000003/sig000000d2 ;
  wire \blk00000003/sig000000d1 ;
  wire \blk00000003/sig000000d0 ;
  wire \blk00000003/sig000000cf ;
  wire \blk00000003/sig000000ce ;
  wire \blk00000003/sig000000cd ;
  wire \blk00000003/sig000000cc ;
  wire \blk00000003/sig000000cb ;
  wire \blk00000003/sig000000ca ;
  wire \blk00000003/sig000000c9 ;
  wire \blk00000003/sig000000c8 ;
  wire \blk00000003/sig000000c7 ;
  wire \blk00000003/sig000000c6 ;
  wire \blk00000003/sig000000c5 ;
  wire \blk00000003/sig000000c4 ;
  wire \blk00000003/sig000000c3 ;
  wire \blk00000003/sig000000c2 ;
  wire \blk00000003/sig000000c1 ;
  wire \blk00000003/sig000000c0 ;
  wire \blk00000003/sig000000bf ;
  wire \blk00000003/sig000000be ;
  wire \blk00000003/sig000000bd ;
  wire \blk00000003/sig000000bc ;
  wire \blk00000003/sig000000bb ;
  wire \blk00000003/sig000000ba ;
  wire \blk00000003/sig000000b9 ;
  wire \blk00000003/sig000000b8 ;
  wire \blk00000003/sig000000b7 ;
  wire \blk00000003/sig000000b6 ;
  wire \blk00000003/sig000000b5 ;
  wire \blk00000003/sig000000b4 ;
  wire \blk00000003/sig000000b3 ;
  wire \blk00000003/sig000000b2 ;
  wire \blk00000003/sig000000b1 ;
  wire \blk00000003/sig000000b0 ;
  wire \blk00000003/sig000000af ;
  wire \blk00000003/sig000000ae ;
  wire \blk00000003/sig000000ad ;
  wire \blk00000003/sig000000ac ;
  wire \blk00000003/sig000000ab ;
  wire \blk00000003/sig000000aa ;
  wire \blk00000003/sig000000a9 ;
  wire \blk00000003/sig000000a8 ;
  wire \blk00000003/sig000000a7 ;
  wire \blk00000003/sig000000a6 ;
  wire \blk00000003/sig000000a5 ;
  wire \blk00000003/sig000000a4 ;
  wire \blk00000003/sig000000a3 ;
  wire \blk00000003/sig000000a2 ;
  wire \blk00000003/sig000000a1 ;
  wire \blk00000003/sig000000a0 ;
  wire \blk00000003/sig0000009f ;
  wire \blk00000003/sig0000009e ;
  wire \blk00000003/sig0000009d ;
  wire \blk00000003/sig0000009c ;
  wire \blk00000003/sig0000009b ;
  wire \blk00000003/sig0000009a ;
  wire \blk00000003/sig00000099 ;
  wire \blk00000003/sig00000098 ;
  wire \blk00000003/sig00000097 ;
  wire \blk00000003/sig00000096 ;
  wire \blk00000003/sig00000095 ;
  wire \blk00000003/sig00000094 ;
  wire \blk00000003/sig00000093 ;
  wire \blk00000003/sig00000092 ;
  wire \blk00000003/sig00000091 ;
  wire \blk00000003/sig00000090 ;
  wire \blk00000003/sig0000008f ;
  wire \blk00000003/sig0000008e ;
  wire \blk00000003/sig0000008d ;
  wire \blk00000003/sig0000008c ;
  wire \blk00000003/sig0000008b ;
  wire \blk00000003/sig0000008a ;
  wire \blk00000003/sig00000089 ;
  wire \blk00000003/sig00000088 ;
  wire \blk00000003/sig00000087 ;
  wire \blk00000003/sig00000086 ;
  wire \blk00000003/sig00000085 ;
  wire \blk00000003/sig00000084 ;
  wire \blk00000003/sig00000083 ;
  wire \blk00000003/sig00000082 ;
  wire \blk00000003/sig00000081 ;
  wire \blk00000003/sig00000080 ;
  wire \blk00000003/sig0000007f ;
  wire \blk00000003/sig0000007e ;
  wire \blk00000003/sig0000007d ;
  wire \blk00000003/sig0000007c ;
  wire \blk00000003/sig0000007b ;
  wire \blk00000003/sig0000007a ;
  wire \blk00000003/sig00000079 ;
  wire \blk00000003/sig00000078 ;
  wire \blk00000003/sig00000077 ;
  wire \blk00000003/sig00000076 ;
  wire \blk00000003/sig00000075 ;
  wire \blk00000003/sig00000074 ;
  wire \blk00000003/sig00000073 ;
  wire \blk00000003/sig00000072 ;
  wire \blk00000003/sig00000071 ;
  wire \blk00000003/sig00000070 ;
  wire \blk00000003/sig0000006f ;
  wire \blk00000003/sig0000006e ;
  wire \blk00000003/sig0000006d ;
  wire \blk00000003/sig0000006c ;
  wire \blk00000003/sig0000006b ;
  wire \blk00000003/sig0000006a ;
  wire \blk00000003/sig00000069 ;
  wire \blk00000003/sig00000068 ;
  wire \blk00000003/sig00000067 ;
  wire \blk00000003/sig00000066 ;
  wire \blk00000003/sig00000065 ;
  wire \blk00000003/sig00000064 ;
  wire \blk00000003/sig00000063 ;
  wire \blk00000003/sig00000062 ;
  wire \blk00000003/sig00000061 ;
  wire \blk00000003/sig00000060 ;
  wire \blk00000003/sig0000005f ;
  wire \blk00000003/sig0000005e ;
  wire \blk00000003/sig0000005d ;
  wire \blk00000003/sig0000005c ;
  wire \blk00000003/sig0000005b ;
  wire \blk00000003/sig0000005a ;
  wire \blk00000003/sig00000059 ;
  wire \blk00000003/sig00000058 ;
  wire \blk00000003/sig00000057 ;
  wire \blk00000003/sig00000056 ;
  wire \blk00000003/sig00000055 ;
  wire \blk00000003/sig00000054 ;
  wire \blk00000003/sig00000053 ;
  wire \blk00000003/sig00000052 ;
  wire \blk00000003/sig00000051 ;
  wire \blk00000003/sig00000050 ;
  wire \blk00000003/sig0000004f ;
  wire \blk00000003/sig0000004e ;
  wire \blk00000003/sig0000004d ;
  wire \blk00000003/sig0000004c ;
  wire \blk00000003/sig0000004b ;
  wire \blk00000003/sig0000004a ;
  wire \blk00000003/sig00000049 ;
  wire \blk00000003/sig00000048 ;
  wire \blk00000003/sig00000047 ;
  wire \blk00000003/sig00000046 ;
  wire \blk00000003/sig00000045 ;
  wire \blk00000003/sig00000044 ;
  wire \blk00000003/sig00000043 ;
  wire \blk00000003/sig00000042 ;
  wire \blk00000003/sig00000041 ;
  wire \blk00000003/sig00000040 ;
  wire \blk00000003/sig0000003f ;
  wire \blk00000003/sig0000003e ;
  wire \blk00000003/sig0000003d ;
  wire \blk00000003/sig0000003c ;
  wire \blk00000003/sig0000003b ;
  wire \blk00000003/sig0000003a ;
  wire \blk00000003/sig00000039 ;
  wire \blk00000003/sig00000038 ;
  wire \blk00000003/sig00000037 ;
  wire \blk00000003/sig00000036 ;
  wire \blk00000003/sig00000035 ;
  wire \blk00000003/sig00000034 ;
  wire \blk00000003/sig00000033 ;
  wire \blk00000003/sig00000032 ;
  wire \blk00000003/sig00000031 ;
  wire \blk00000003/sig00000030 ;
  wire \blk00000003/sig0000002f ;
  wire \blk00000003/sig0000002e ;
  wire \blk00000003/sig0000002d ;
  wire \blk00000003/sig0000002c ;
  wire \blk00000003/sig0000002b ;
  wire \blk00000003/sig0000002a ;
  wire \blk00000003/sig00000029 ;
  wire \blk00000003/sig00000028 ;
  wire \blk00000003/sig00000027 ;
  wire \blk00000003/sig00000026 ;
  wire \blk00000003/sig00000025 ;
  wire \blk00000003/sig00000024 ;
  wire \blk00000003/sig00000023 ;
  wire \blk00000003/sig00000022 ;
  wire \blk00000003/sig00000021 ;
  wire \blk00000003/sig00000020 ;
  wire \blk00000003/sig0000001f ;
  wire \blk00000003/sig0000001e ;
  wire \blk00000003/sig0000001d ;
  wire \blk00000003/sig0000001c ;
  wire \blk00000003/sig0000001a ;
  wire NLW_blk00000001_P_UNCONNECTED;
  wire NLW_blk00000002_G_UNCONNECTED;
  wire [7 : 0] dividend_0;
  wire [3 : 0] divisor_1;
  wire [7 : 0] quotient_2;
  wire [3 : 0] fractional_3;
  assign
    dividend_0[7] = dividend[7],
    dividend_0[6] = dividend[6],
    dividend_0[5] = dividend[5],
    dividend_0[4] = dividend[4],
    dividend_0[3] = dividend[3],
    dividend_0[2] = dividend[2],
    dividend_0[1] = dividend[1],
    dividend_0[0] = dividend[0],
    quotient[7] = quotient_2[7],
    quotient[6] = quotient_2[6],
    quotient[5] = quotient_2[5],
    quotient[4] = quotient_2[4],
    quotient[3] = quotient_2[3],
    quotient[2] = quotient_2[2],
    quotient[1] = quotient_2[1],
    quotient[0] = quotient_2[0],
    divisor_1[3] = divisor[3],
    divisor_1[2] = divisor[2],
    divisor_1[1] = divisor[1],
    divisor_1[0] = divisor[0],
    rfd = NlwRenamedSig_OI_rfd,
    fractional[3] = fractional_3[3],
    fractional[2] = fractional_3[2],
    fractional[1] = fractional_3[1],
    fractional[0] = fractional_3[0];
  VCC   blk00000001 (
    .P(NLW_blk00000001_P_UNCONNECTED)
  );
  GND   blk00000002 (
    .G(NLW_blk00000002_G_UNCONNECTED)
  );
  INV   \blk00000003/blk00000140  (
    .I(\blk00000003/sig00000065 ),
    .O(\blk00000003/sig00000092 )
  );
  INV   \blk00000003/blk0000013f  (
    .I(\blk00000003/sig00000064 ),
    .O(\blk00000003/sig00000095 )
  );
  INV   \blk00000003/blk0000013e  (
    .I(\blk00000003/sig00000063 ),
    .O(\blk00000003/sig00000098 )
  );
  INV   \blk00000003/blk0000013d  (
    .I(\blk00000003/sig00000029 ),
    .O(\blk00000003/sig00000086 )
  );
  INV   \blk00000003/blk0000013c  (
    .I(\blk00000003/sig00000027 ),
    .O(\blk00000003/sig00000087 )
  );
  INV   \blk00000003/blk0000013b  (
    .I(\blk00000003/sig00000025 ),
    .O(\blk00000003/sig00000088 )
  );
  INV   \blk00000003/blk0000013a  (
    .I(\blk00000003/sig00000023 ),
    .O(\blk00000003/sig00000089 )
  );
  INV   \blk00000003/blk00000139  (
    .I(\blk00000003/sig00000021 ),
    .O(\blk00000003/sig0000008a )
  );
  INV   \blk00000003/blk00000138  (
    .I(\blk00000003/sig0000001f ),
    .O(\blk00000003/sig0000008b )
  );
  INV   \blk00000003/blk00000137  (
    .I(\blk00000003/sig0000001d ),
    .O(\blk00000003/sig0000008c )
  );
  INV   \blk00000003/blk00000136  (
    .I(\blk00000003/sig0000001c ),
    .O(\blk00000003/sig0000012d )
  );
  INV   \blk00000003/blk00000135  (
    .I(\blk00000003/sig0000002a ),
    .O(\blk00000003/sig00000118 )
  );
  INV   \blk00000003/blk00000134  (
    .I(\blk00000003/sig00000060 ),
    .O(\blk00000003/sig00000103 )
  );
  INV   \blk00000003/blk00000133  (
    .I(\blk00000003/sig00000038 ),
    .O(\blk00000003/sig000000ee )
  );
  INV   \blk00000003/blk00000132  (
    .I(\blk00000003/sig00000045 ),
    .O(\blk00000003/sig000000d9 )
  );
  INV   \blk00000003/blk00000131  (
    .I(\blk00000003/sig0000004e ),
    .O(\blk00000003/sig000000c4 )
  );
  INV   \blk00000003/blk00000130  (
    .I(\blk00000003/sig00000057 ),
    .O(\blk00000003/sig000000af )
  );
  LUT3 #(
    .INIT ( 8'h6A ))
  \blk00000003/blk0000012f  (
    .I0(\blk00000003/sig00000133 ),
    .I1(\blk00000003/sig00000085 ),
    .I2(\blk00000003/sig00000134 ),
    .O(\blk00000003/sig00000137 )
  );
  LUT3 #(
    .INIT ( 8'h6A ))
  \blk00000003/blk0000012e  (
    .I0(\blk00000003/sig00000132 ),
    .I1(\blk00000003/sig00000084 ),
    .I2(\blk00000003/sig00000134 ),
    .O(\blk00000003/sig0000013c )
  );
  LUT3 #(
    .INIT ( 8'h6A ))
  \blk00000003/blk0000012d  (
    .I0(\blk00000003/sig00000131 ),
    .I1(\blk00000003/sig00000083 ),
    .I2(\blk00000003/sig00000134 ),
    .O(\blk00000003/sig00000140 )
  );
  LUT3 #(
    .INIT ( 8'h6A ))
  \blk00000003/blk0000012c  (
    .I0(\blk00000003/sig00000130 ),
    .I1(\blk00000003/sig00000082 ),
    .I2(\blk00000003/sig00000134 ),
    .O(\blk00000003/sig00000146 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000003/blk0000012b  (
    .I0(\blk00000003/sig0000011e ),
    .I1(\blk00000003/sig0000001c ),
    .O(\blk00000003/sig00000121 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000012a  (
    .I0(\blk00000003/sig0000011d ),
    .I1(\blk00000003/sig00000081 ),
    .I2(\blk00000003/sig0000001c ),
    .O(\blk00000003/sig00000124 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000129  (
    .I0(\blk00000003/sig0000011c ),
    .I1(\blk00000003/sig00000080 ),
    .I2(\blk00000003/sig0000001c ),
    .O(\blk00000003/sig00000127 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000128  (
    .I0(\blk00000003/sig0000011b ),
    .I1(\blk00000003/sig0000007f ),
    .I2(\blk00000003/sig0000001c ),
    .O(\blk00000003/sig0000012a )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000127  (
    .I0(\blk00000003/sig00000061 ),
    .I1(\blk00000003/sig0000007e ),
    .I2(\blk00000003/sig0000001c ),
    .O(\blk00000003/sig0000012e )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000003/blk00000126  (
    .I0(\blk00000003/sig00000109 ),
    .I1(\blk00000003/sig0000002a ),
    .O(\blk00000003/sig0000010c )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000125  (
    .I0(\blk00000003/sig00000108 ),
    .I1(\blk00000003/sig0000007d ),
    .I2(\blk00000003/sig0000002a ),
    .O(\blk00000003/sig0000010f )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000124  (
    .I0(\blk00000003/sig00000107 ),
    .I1(\blk00000003/sig0000007c ),
    .I2(\blk00000003/sig0000002a ),
    .O(\blk00000003/sig00000112 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000123  (
    .I0(\blk00000003/sig00000106 ),
    .I1(\blk00000003/sig0000007b ),
    .I2(\blk00000003/sig0000002a ),
    .O(\blk00000003/sig00000115 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000122  (
    .I0(\blk00000003/sig00000033 ),
    .I1(\blk00000003/sig0000007a ),
    .I2(\blk00000003/sig0000002a ),
    .O(\blk00000003/sig00000119 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000003/blk00000121  (
    .I0(\blk00000003/sig000000f4 ),
    .I1(\blk00000003/sig00000060 ),
    .O(\blk00000003/sig000000f7 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000120  (
    .I0(\blk00000003/sig000000f3 ),
    .I1(\blk00000003/sig00000079 ),
    .I2(\blk00000003/sig00000060 ),
    .O(\blk00000003/sig000000fa )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000011f  (
    .I0(\blk00000003/sig000000f2 ),
    .I1(\blk00000003/sig00000078 ),
    .I2(\blk00000003/sig00000060 ),
    .O(\blk00000003/sig000000fd )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000011e  (
    .I0(\blk00000003/sig000000f1 ),
    .I1(\blk00000003/sig00000077 ),
    .I2(\blk00000003/sig00000060 ),
    .O(\blk00000003/sig00000100 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000011d  (
    .I0(\blk00000003/sig00000037 ),
    .I1(\blk00000003/sig00000076 ),
    .I2(\blk00000003/sig00000060 ),
    .O(\blk00000003/sig00000104 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000003/blk0000011c  (
    .I0(\blk00000003/sig000000df ),
    .I1(\blk00000003/sig00000038 ),
    .O(\blk00000003/sig000000e2 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000011b  (
    .I0(\blk00000003/sig000000de ),
    .I1(\blk00000003/sig00000075 ),
    .I2(\blk00000003/sig00000038 ),
    .O(\blk00000003/sig000000e5 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000011a  (
    .I0(\blk00000003/sig000000dd ),
    .I1(\blk00000003/sig00000074 ),
    .I2(\blk00000003/sig00000038 ),
    .O(\blk00000003/sig000000e8 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000119  (
    .I0(\blk00000003/sig000000dc ),
    .I1(\blk00000003/sig00000073 ),
    .I2(\blk00000003/sig00000038 ),
    .O(\blk00000003/sig000000eb )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000118  (
    .I0(\blk00000003/sig00000044 ),
    .I1(\blk00000003/sig00000072 ),
    .I2(\blk00000003/sig00000038 ),
    .O(\blk00000003/sig000000ef )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000003/blk00000117  (
    .I0(\blk00000003/sig000000ca ),
    .I1(\blk00000003/sig00000045 ),
    .O(\blk00000003/sig000000cd )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000116  (
    .I0(\blk00000003/sig000000c9 ),
    .I1(\blk00000003/sig00000071 ),
    .I2(\blk00000003/sig00000045 ),
    .O(\blk00000003/sig000000d0 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000115  (
    .I0(\blk00000003/sig000000c8 ),
    .I1(\blk00000003/sig00000070 ),
    .I2(\blk00000003/sig00000045 ),
    .O(\blk00000003/sig000000d3 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000114  (
    .I0(\blk00000003/sig000000c7 ),
    .I1(\blk00000003/sig0000006f ),
    .I2(\blk00000003/sig00000045 ),
    .O(\blk00000003/sig000000d6 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000113  (
    .I0(\blk00000003/sig0000004d ),
    .I1(\blk00000003/sig0000006e ),
    .I2(\blk00000003/sig00000045 ),
    .O(\blk00000003/sig000000da )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000003/blk00000112  (
    .I0(\blk00000003/sig000000b5 ),
    .I1(\blk00000003/sig0000004e ),
    .O(\blk00000003/sig000000b8 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000111  (
    .I0(\blk00000003/sig000000b4 ),
    .I1(\blk00000003/sig0000006d ),
    .I2(\blk00000003/sig0000004e ),
    .O(\blk00000003/sig000000bb )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000110  (
    .I0(\blk00000003/sig000000b3 ),
    .I1(\blk00000003/sig0000006c ),
    .I2(\blk00000003/sig0000004e ),
    .O(\blk00000003/sig000000be )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000010f  (
    .I0(\blk00000003/sig000000b2 ),
    .I1(\blk00000003/sig0000006b ),
    .I2(\blk00000003/sig0000004e ),
    .O(\blk00000003/sig000000c1 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000010e  (
    .I0(\blk00000003/sig00000056 ),
    .I1(\blk00000003/sig0000006a ),
    .I2(\blk00000003/sig0000004e ),
    .O(\blk00000003/sig000000c5 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000003/blk0000010d  (
    .I0(\blk00000003/sig000000a0 ),
    .I1(\blk00000003/sig00000057 ),
    .O(\blk00000003/sig000000a3 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000010c  (
    .I0(\blk00000003/sig0000009f ),
    .I1(\blk00000003/sig00000069 ),
    .I2(\blk00000003/sig00000057 ),
    .O(\blk00000003/sig000000a6 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000010b  (
    .I0(\blk00000003/sig0000009e ),
    .I1(\blk00000003/sig00000068 ),
    .I2(\blk00000003/sig00000057 ),
    .O(\blk00000003/sig000000a9 )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk0000010a  (
    .I0(\blk00000003/sig0000009d ),
    .I1(\blk00000003/sig00000067 ),
    .I2(\blk00000003/sig00000057 ),
    .O(\blk00000003/sig000000ac )
  );
  LUT3 #(
    .INIT ( 8'h69 ))
  \blk00000003/blk00000109  (
    .I0(\blk00000003/sig0000005f ),
    .I1(\blk00000003/sig00000066 ),
    .I2(\blk00000003/sig00000057 ),
    .O(\blk00000003/sig000000b0 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000003/blk00000108  (
    .I0(\blk00000003/sig0000008e ),
    .I1(\blk00000003/sig00000062 ),
    .O(\blk00000003/sig0000009b )
  );
  LUT1 #(
    .INIT ( 2'h1 ))
  \blk00000003/blk00000107  (
    .I0(\blk00000003/sig00000134 ),
    .O(\blk00000003/sig0000008d )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000106  (
    .C(clk),
    .D(\blk00000003/sig00000144 ),
    .Q(\blk00000003/sig0000014a )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000105  (
    .C(clk),
    .D(\blk00000003/sig00000145 ),
    .Q(\blk00000003/sig00000149 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000104  (
    .C(clk),
    .D(\blk00000003/sig00000138 ),
    .Q(fractional_3[3])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000103  (
    .C(clk),
    .D(\blk00000003/sig0000013d ),
    .Q(fractional_3[2])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000102  (
    .C(clk),
    .D(\blk00000003/sig00000141 ),
    .Q(fractional_3[1])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000101  (
    .C(clk),
    .D(\blk00000003/sig00000147 ),
    .Q(fractional_3[0])
  );
  MULT_AND   \blk00000003/blk00000100  (
    .I0(\blk00000003/sig00000082 ),
    .I1(\blk00000003/sig00000134 ),
    .LO(\blk00000003/sig00000148 )
  );
  MULT_AND   \blk00000003/blk000000ff  (
    .I0(\blk00000003/sig00000083 ),
    .I1(\blk00000003/sig00000134 ),
    .LO(\blk00000003/sig00000142 )
  );
  MULT_AND   \blk00000003/blk000000fe  (
    .I0(\blk00000003/sig00000084 ),
    .I1(\blk00000003/sig00000134 ),
    .LO(\blk00000003/sig0000013e )
  );
  MULT_AND   \blk00000003/blk000000fd  (
    .I0(\blk00000003/sig00000085 ),
    .I1(\blk00000003/sig00000134 ),
    .LO(\blk00000003/sig00000139 )
  );
  MULT_AND   \blk00000003/blk000000fc  (
    .I0(\blk00000003/sig0000001a ),
    .I1(\blk00000003/sig00000134 ),
    .LO(\blk00000003/sig00000143 )
  );
  MUXCY   \blk00000003/blk000000fb  (
    .CI(\blk00000003/sig0000001a ),
    .DI(\blk00000003/sig00000148 ),
    .S(\blk00000003/sig00000146 ),
    .O(\blk00000003/sig0000013f )
  );
  XORCY   \blk00000003/blk000000fa  (
    .CI(\blk00000003/sig0000001a ),
    .LI(\blk00000003/sig00000146 ),
    .O(\blk00000003/sig00000147 )
  );
  XORCY   \blk00000003/blk000000f9  (
    .CI(\blk00000003/sig0000013a ),
    .LI(\blk00000003/sig0000001a ),
    .O(\blk00000003/sig00000145 )
  );
  MUXCY   \blk00000003/blk000000f8  (
    .CI(\blk00000003/sig0000013a ),
    .DI(\blk00000003/sig00000143 ),
    .S(\blk00000003/sig0000001a ),
    .O(\blk00000003/sig00000144 )
  );
  MUXCY   \blk00000003/blk000000f7  (
    .CI(\blk00000003/sig0000013f ),
    .DI(\blk00000003/sig00000142 ),
    .S(\blk00000003/sig00000140 ),
    .O(\blk00000003/sig0000013b )
  );
  XORCY   \blk00000003/blk000000f6  (
    .CI(\blk00000003/sig0000013f ),
    .LI(\blk00000003/sig00000140 ),
    .O(\blk00000003/sig00000141 )
  );
  MUXCY   \blk00000003/blk000000f5  (
    .CI(\blk00000003/sig0000013b ),
    .DI(\blk00000003/sig0000013e ),
    .S(\blk00000003/sig0000013c ),
    .O(\blk00000003/sig00000136 )
  );
  XORCY   \blk00000003/blk000000f4  (
    .CI(\blk00000003/sig0000013b ),
    .LI(\blk00000003/sig0000013c ),
    .O(\blk00000003/sig0000013d )
  );
  MUXCY   \blk00000003/blk000000f3  (
    .CI(\blk00000003/sig00000136 ),
    .DI(\blk00000003/sig00000139 ),
    .S(\blk00000003/sig00000137 ),
    .O(\blk00000003/sig0000013a )
  );
  XORCY   \blk00000003/blk000000f2  (
    .CI(\blk00000003/sig00000136 ),
    .LI(\blk00000003/sig00000137 ),
    .O(\blk00000003/sig00000138 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk000000f1  (
    .C(clk),
    .D(\blk00000003/sig0000012c ),
    .Q(\blk00000003/sig00000135 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000f0  (
    .C(clk),
    .D(\blk00000003/sig00000122 ),
    .Q(\blk00000003/sig00000134 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000ef  (
    .C(clk),
    .D(\blk00000003/sig00000125 ),
    .Q(\blk00000003/sig00000133 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000ee  (
    .C(clk),
    .D(\blk00000003/sig00000128 ),
    .Q(\blk00000003/sig00000132 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000ed  (
    .C(clk),
    .D(\blk00000003/sig0000012b ),
    .Q(\blk00000003/sig00000131 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000ec  (
    .C(clk),
    .D(\blk00000003/sig0000012f ),
    .Q(\blk00000003/sig00000130 )
  );
  MUXCY   \blk00000003/blk000000eb  (
    .CI(\blk00000003/sig0000012d ),
    .DI(\blk00000003/sig00000061 ),
    .S(\blk00000003/sig0000012e ),
    .O(\blk00000003/sig00000129 )
  );
  XORCY   \blk00000003/blk000000ea  (
    .CI(\blk00000003/sig0000012d ),
    .LI(\blk00000003/sig0000012e ),
    .O(\blk00000003/sig0000012f )
  );
  MUXCY   \blk00000003/blk000000e9  (
    .CI(\blk00000003/sig00000120 ),
    .DI(\blk00000003/sig0000011e ),
    .S(\blk00000003/sig00000121 ),
    .O(\blk00000003/sig0000012c )
  );
  MUXCY   \blk00000003/blk000000e8  (
    .CI(\blk00000003/sig00000129 ),
    .DI(\blk00000003/sig0000011b ),
    .S(\blk00000003/sig0000012a ),
    .O(\blk00000003/sig00000126 )
  );
  MUXCY   \blk00000003/blk000000e7  (
    .CI(\blk00000003/sig00000126 ),
    .DI(\blk00000003/sig0000011c ),
    .S(\blk00000003/sig00000127 ),
    .O(\blk00000003/sig00000123 )
  );
  MUXCY   \blk00000003/blk000000e6  (
    .CI(\blk00000003/sig00000123 ),
    .DI(\blk00000003/sig0000011d ),
    .S(\blk00000003/sig00000124 ),
    .O(\blk00000003/sig00000120 )
  );
  XORCY   \blk00000003/blk000000e5  (
    .CI(\blk00000003/sig00000129 ),
    .LI(\blk00000003/sig0000012a ),
    .O(\blk00000003/sig0000012b )
  );
  XORCY   \blk00000003/blk000000e4  (
    .CI(\blk00000003/sig00000126 ),
    .LI(\blk00000003/sig00000127 ),
    .O(\blk00000003/sig00000128 )
  );
  XORCY   \blk00000003/blk000000e3  (
    .CI(\blk00000003/sig00000123 ),
    .LI(\blk00000003/sig00000124 ),
    .O(\blk00000003/sig00000125 )
  );
  XORCY   \blk00000003/blk000000e2  (
    .CI(\blk00000003/sig00000120 ),
    .LI(\blk00000003/sig00000121 ),
    .O(\blk00000003/sig00000122 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk000000e1  (
    .C(clk),
    .D(\blk00000003/sig00000117 ),
    .Q(\blk00000003/sig0000011f )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000e0  (
    .C(clk),
    .D(\blk00000003/sig0000010d ),
    .Q(\blk00000003/sig0000001c )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000df  (
    .C(clk),
    .D(\blk00000003/sig00000110 ),
    .Q(\blk00000003/sig0000011e )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000de  (
    .C(clk),
    .D(\blk00000003/sig00000113 ),
    .Q(\blk00000003/sig0000011d )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000dd  (
    .C(clk),
    .D(\blk00000003/sig00000116 ),
    .Q(\blk00000003/sig0000011c )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000dc  (
    .C(clk),
    .D(\blk00000003/sig0000011a ),
    .Q(\blk00000003/sig0000011b )
  );
  MUXCY   \blk00000003/blk000000db  (
    .CI(\blk00000003/sig00000118 ),
    .DI(\blk00000003/sig00000033 ),
    .S(\blk00000003/sig00000119 ),
    .O(\blk00000003/sig00000114 )
  );
  XORCY   \blk00000003/blk000000da  (
    .CI(\blk00000003/sig00000118 ),
    .LI(\blk00000003/sig00000119 ),
    .O(\blk00000003/sig0000011a )
  );
  MUXCY   \blk00000003/blk000000d9  (
    .CI(\blk00000003/sig0000010b ),
    .DI(\blk00000003/sig00000109 ),
    .S(\blk00000003/sig0000010c ),
    .O(\blk00000003/sig00000117 )
  );
  MUXCY   \blk00000003/blk000000d8  (
    .CI(\blk00000003/sig00000114 ),
    .DI(\blk00000003/sig00000106 ),
    .S(\blk00000003/sig00000115 ),
    .O(\blk00000003/sig00000111 )
  );
  MUXCY   \blk00000003/blk000000d7  (
    .CI(\blk00000003/sig00000111 ),
    .DI(\blk00000003/sig00000107 ),
    .S(\blk00000003/sig00000112 ),
    .O(\blk00000003/sig0000010e )
  );
  MUXCY   \blk00000003/blk000000d6  (
    .CI(\blk00000003/sig0000010e ),
    .DI(\blk00000003/sig00000108 ),
    .S(\blk00000003/sig0000010f ),
    .O(\blk00000003/sig0000010b )
  );
  XORCY   \blk00000003/blk000000d5  (
    .CI(\blk00000003/sig00000114 ),
    .LI(\blk00000003/sig00000115 ),
    .O(\blk00000003/sig00000116 )
  );
  XORCY   \blk00000003/blk000000d4  (
    .CI(\blk00000003/sig00000111 ),
    .LI(\blk00000003/sig00000112 ),
    .O(\blk00000003/sig00000113 )
  );
  XORCY   \blk00000003/blk000000d3  (
    .CI(\blk00000003/sig0000010e ),
    .LI(\blk00000003/sig0000010f ),
    .O(\blk00000003/sig00000110 )
  );
  XORCY   \blk00000003/blk000000d2  (
    .CI(\blk00000003/sig0000010b ),
    .LI(\blk00000003/sig0000010c ),
    .O(\blk00000003/sig0000010d )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk000000d1  (
    .C(clk),
    .D(\blk00000003/sig00000102 ),
    .Q(\blk00000003/sig0000010a )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000d0  (
    .C(clk),
    .D(\blk00000003/sig000000f8 ),
    .Q(\blk00000003/sig0000002a )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000cf  (
    .C(clk),
    .D(\blk00000003/sig000000fb ),
    .Q(\blk00000003/sig00000109 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000ce  (
    .C(clk),
    .D(\blk00000003/sig000000fe ),
    .Q(\blk00000003/sig00000108 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000cd  (
    .C(clk),
    .D(\blk00000003/sig00000101 ),
    .Q(\blk00000003/sig00000107 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000cc  (
    .C(clk),
    .D(\blk00000003/sig00000105 ),
    .Q(\blk00000003/sig00000106 )
  );
  MUXCY   \blk00000003/blk000000cb  (
    .CI(\blk00000003/sig00000103 ),
    .DI(\blk00000003/sig00000037 ),
    .S(\blk00000003/sig00000104 ),
    .O(\blk00000003/sig000000ff )
  );
  XORCY   \blk00000003/blk000000ca  (
    .CI(\blk00000003/sig00000103 ),
    .LI(\blk00000003/sig00000104 ),
    .O(\blk00000003/sig00000105 )
  );
  MUXCY   \blk00000003/blk000000c9  (
    .CI(\blk00000003/sig000000f6 ),
    .DI(\blk00000003/sig000000f4 ),
    .S(\blk00000003/sig000000f7 ),
    .O(\blk00000003/sig00000102 )
  );
  MUXCY   \blk00000003/blk000000c8  (
    .CI(\blk00000003/sig000000ff ),
    .DI(\blk00000003/sig000000f1 ),
    .S(\blk00000003/sig00000100 ),
    .O(\blk00000003/sig000000fc )
  );
  MUXCY   \blk00000003/blk000000c7  (
    .CI(\blk00000003/sig000000fc ),
    .DI(\blk00000003/sig000000f2 ),
    .S(\blk00000003/sig000000fd ),
    .O(\blk00000003/sig000000f9 )
  );
  MUXCY   \blk00000003/blk000000c6  (
    .CI(\blk00000003/sig000000f9 ),
    .DI(\blk00000003/sig000000f3 ),
    .S(\blk00000003/sig000000fa ),
    .O(\blk00000003/sig000000f6 )
  );
  XORCY   \blk00000003/blk000000c5  (
    .CI(\blk00000003/sig000000ff ),
    .LI(\blk00000003/sig00000100 ),
    .O(\blk00000003/sig00000101 )
  );
  XORCY   \blk00000003/blk000000c4  (
    .CI(\blk00000003/sig000000fc ),
    .LI(\blk00000003/sig000000fd ),
    .O(\blk00000003/sig000000fe )
  );
  XORCY   \blk00000003/blk000000c3  (
    .CI(\blk00000003/sig000000f9 ),
    .LI(\blk00000003/sig000000fa ),
    .O(\blk00000003/sig000000fb )
  );
  XORCY   \blk00000003/blk000000c2  (
    .CI(\blk00000003/sig000000f6 ),
    .LI(\blk00000003/sig000000f7 ),
    .O(\blk00000003/sig000000f8 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk000000c1  (
    .C(clk),
    .D(\blk00000003/sig000000ed ),
    .Q(\blk00000003/sig000000f5 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000c0  (
    .C(clk),
    .D(\blk00000003/sig000000e3 ),
    .Q(\blk00000003/sig00000060 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000bf  (
    .C(clk),
    .D(\blk00000003/sig000000e6 ),
    .Q(\blk00000003/sig000000f4 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000be  (
    .C(clk),
    .D(\blk00000003/sig000000e9 ),
    .Q(\blk00000003/sig000000f3 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000bd  (
    .C(clk),
    .D(\blk00000003/sig000000ec ),
    .Q(\blk00000003/sig000000f2 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000bc  (
    .C(clk),
    .D(\blk00000003/sig000000f0 ),
    .Q(\blk00000003/sig000000f1 )
  );
  MUXCY   \blk00000003/blk000000bb  (
    .CI(\blk00000003/sig000000ee ),
    .DI(\blk00000003/sig00000044 ),
    .S(\blk00000003/sig000000ef ),
    .O(\blk00000003/sig000000ea )
  );
  XORCY   \blk00000003/blk000000ba  (
    .CI(\blk00000003/sig000000ee ),
    .LI(\blk00000003/sig000000ef ),
    .O(\blk00000003/sig000000f0 )
  );
  MUXCY   \blk00000003/blk000000b9  (
    .CI(\blk00000003/sig000000e1 ),
    .DI(\blk00000003/sig000000df ),
    .S(\blk00000003/sig000000e2 ),
    .O(\blk00000003/sig000000ed )
  );
  MUXCY   \blk00000003/blk000000b8  (
    .CI(\blk00000003/sig000000ea ),
    .DI(\blk00000003/sig000000dc ),
    .S(\blk00000003/sig000000eb ),
    .O(\blk00000003/sig000000e7 )
  );
  MUXCY   \blk00000003/blk000000b7  (
    .CI(\blk00000003/sig000000e7 ),
    .DI(\blk00000003/sig000000dd ),
    .S(\blk00000003/sig000000e8 ),
    .O(\blk00000003/sig000000e4 )
  );
  MUXCY   \blk00000003/blk000000b6  (
    .CI(\blk00000003/sig000000e4 ),
    .DI(\blk00000003/sig000000de ),
    .S(\blk00000003/sig000000e5 ),
    .O(\blk00000003/sig000000e1 )
  );
  XORCY   \blk00000003/blk000000b5  (
    .CI(\blk00000003/sig000000ea ),
    .LI(\blk00000003/sig000000eb ),
    .O(\blk00000003/sig000000ec )
  );
  XORCY   \blk00000003/blk000000b4  (
    .CI(\blk00000003/sig000000e7 ),
    .LI(\blk00000003/sig000000e8 ),
    .O(\blk00000003/sig000000e9 )
  );
  XORCY   \blk00000003/blk000000b3  (
    .CI(\blk00000003/sig000000e4 ),
    .LI(\blk00000003/sig000000e5 ),
    .O(\blk00000003/sig000000e6 )
  );
  XORCY   \blk00000003/blk000000b2  (
    .CI(\blk00000003/sig000000e1 ),
    .LI(\blk00000003/sig000000e2 ),
    .O(\blk00000003/sig000000e3 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk000000b1  (
    .C(clk),
    .D(\blk00000003/sig000000d8 ),
    .Q(\blk00000003/sig000000e0 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000b0  (
    .C(clk),
    .D(\blk00000003/sig000000ce ),
    .Q(\blk00000003/sig00000038 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000af  (
    .C(clk),
    .D(\blk00000003/sig000000d1 ),
    .Q(\blk00000003/sig000000df )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000ae  (
    .C(clk),
    .D(\blk00000003/sig000000d4 ),
    .Q(\blk00000003/sig000000de )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000ad  (
    .C(clk),
    .D(\blk00000003/sig000000d7 ),
    .Q(\blk00000003/sig000000dd )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000ac  (
    .C(clk),
    .D(\blk00000003/sig000000db ),
    .Q(\blk00000003/sig000000dc )
  );
  MUXCY   \blk00000003/blk000000ab  (
    .CI(\blk00000003/sig000000d9 ),
    .DI(\blk00000003/sig0000004d ),
    .S(\blk00000003/sig000000da ),
    .O(\blk00000003/sig000000d5 )
  );
  XORCY   \blk00000003/blk000000aa  (
    .CI(\blk00000003/sig000000d9 ),
    .LI(\blk00000003/sig000000da ),
    .O(\blk00000003/sig000000db )
  );
  MUXCY   \blk00000003/blk000000a9  (
    .CI(\blk00000003/sig000000cc ),
    .DI(\blk00000003/sig000000ca ),
    .S(\blk00000003/sig000000cd ),
    .O(\blk00000003/sig000000d8 )
  );
  MUXCY   \blk00000003/blk000000a8  (
    .CI(\blk00000003/sig000000d5 ),
    .DI(\blk00000003/sig000000c7 ),
    .S(\blk00000003/sig000000d6 ),
    .O(\blk00000003/sig000000d2 )
  );
  MUXCY   \blk00000003/blk000000a7  (
    .CI(\blk00000003/sig000000d2 ),
    .DI(\blk00000003/sig000000c8 ),
    .S(\blk00000003/sig000000d3 ),
    .O(\blk00000003/sig000000cf )
  );
  MUXCY   \blk00000003/blk000000a6  (
    .CI(\blk00000003/sig000000cf ),
    .DI(\blk00000003/sig000000c9 ),
    .S(\blk00000003/sig000000d0 ),
    .O(\blk00000003/sig000000cc )
  );
  XORCY   \blk00000003/blk000000a5  (
    .CI(\blk00000003/sig000000d5 ),
    .LI(\blk00000003/sig000000d6 ),
    .O(\blk00000003/sig000000d7 )
  );
  XORCY   \blk00000003/blk000000a4  (
    .CI(\blk00000003/sig000000d2 ),
    .LI(\blk00000003/sig000000d3 ),
    .O(\blk00000003/sig000000d4 )
  );
  XORCY   \blk00000003/blk000000a3  (
    .CI(\blk00000003/sig000000cf ),
    .LI(\blk00000003/sig000000d0 ),
    .O(\blk00000003/sig000000d1 )
  );
  XORCY   \blk00000003/blk000000a2  (
    .CI(\blk00000003/sig000000cc ),
    .LI(\blk00000003/sig000000cd ),
    .O(\blk00000003/sig000000ce )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk000000a1  (
    .C(clk),
    .D(\blk00000003/sig000000c3 ),
    .Q(\blk00000003/sig000000cb )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk000000a0  (
    .C(clk),
    .D(\blk00000003/sig000000b9 ),
    .Q(\blk00000003/sig00000045 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000009f  (
    .C(clk),
    .D(\blk00000003/sig000000bc ),
    .Q(\blk00000003/sig000000ca )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000009e  (
    .C(clk),
    .D(\blk00000003/sig000000bf ),
    .Q(\blk00000003/sig000000c9 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000009d  (
    .C(clk),
    .D(\blk00000003/sig000000c2 ),
    .Q(\blk00000003/sig000000c8 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000009c  (
    .C(clk),
    .D(\blk00000003/sig000000c6 ),
    .Q(\blk00000003/sig000000c7 )
  );
  MUXCY   \blk00000003/blk0000009b  (
    .CI(\blk00000003/sig000000c4 ),
    .DI(\blk00000003/sig00000056 ),
    .S(\blk00000003/sig000000c5 ),
    .O(\blk00000003/sig000000c0 )
  );
  XORCY   \blk00000003/blk0000009a  (
    .CI(\blk00000003/sig000000c4 ),
    .LI(\blk00000003/sig000000c5 ),
    .O(\blk00000003/sig000000c6 )
  );
  MUXCY   \blk00000003/blk00000099  (
    .CI(\blk00000003/sig000000b7 ),
    .DI(\blk00000003/sig000000b5 ),
    .S(\blk00000003/sig000000b8 ),
    .O(\blk00000003/sig000000c3 )
  );
  MUXCY   \blk00000003/blk00000098  (
    .CI(\blk00000003/sig000000c0 ),
    .DI(\blk00000003/sig000000b2 ),
    .S(\blk00000003/sig000000c1 ),
    .O(\blk00000003/sig000000bd )
  );
  MUXCY   \blk00000003/blk00000097  (
    .CI(\blk00000003/sig000000bd ),
    .DI(\blk00000003/sig000000b3 ),
    .S(\blk00000003/sig000000be ),
    .O(\blk00000003/sig000000ba )
  );
  MUXCY   \blk00000003/blk00000096  (
    .CI(\blk00000003/sig000000ba ),
    .DI(\blk00000003/sig000000b4 ),
    .S(\blk00000003/sig000000bb ),
    .O(\blk00000003/sig000000b7 )
  );
  XORCY   \blk00000003/blk00000095  (
    .CI(\blk00000003/sig000000c0 ),
    .LI(\blk00000003/sig000000c1 ),
    .O(\blk00000003/sig000000c2 )
  );
  XORCY   \blk00000003/blk00000094  (
    .CI(\blk00000003/sig000000bd ),
    .LI(\blk00000003/sig000000be ),
    .O(\blk00000003/sig000000bf )
  );
  XORCY   \blk00000003/blk00000093  (
    .CI(\blk00000003/sig000000ba ),
    .LI(\blk00000003/sig000000bb ),
    .O(\blk00000003/sig000000bc )
  );
  XORCY   \blk00000003/blk00000092  (
    .CI(\blk00000003/sig000000b7 ),
    .LI(\blk00000003/sig000000b8 ),
    .O(\blk00000003/sig000000b9 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000091  (
    .C(clk),
    .D(\blk00000003/sig000000ae ),
    .Q(\blk00000003/sig000000b6 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000090  (
    .C(clk),
    .D(\blk00000003/sig000000a4 ),
    .Q(\blk00000003/sig0000004e )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000008f  (
    .C(clk),
    .D(\blk00000003/sig000000a7 ),
    .Q(\blk00000003/sig000000b5 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000008e  (
    .C(clk),
    .D(\blk00000003/sig000000aa ),
    .Q(\blk00000003/sig000000b4 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000008d  (
    .C(clk),
    .D(\blk00000003/sig000000ad ),
    .Q(\blk00000003/sig000000b3 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000008c  (
    .C(clk),
    .D(\blk00000003/sig000000b1 ),
    .Q(\blk00000003/sig000000b2 )
  );
  MUXCY   \blk00000003/blk0000008b  (
    .CI(\blk00000003/sig000000af ),
    .DI(\blk00000003/sig0000005f ),
    .S(\blk00000003/sig000000b0 ),
    .O(\blk00000003/sig000000ab )
  );
  XORCY   \blk00000003/blk0000008a  (
    .CI(\blk00000003/sig000000af ),
    .LI(\blk00000003/sig000000b0 ),
    .O(\blk00000003/sig000000b1 )
  );
  MUXCY   \blk00000003/blk00000089  (
    .CI(\blk00000003/sig000000a2 ),
    .DI(\blk00000003/sig000000a0 ),
    .S(\blk00000003/sig000000a3 ),
    .O(\blk00000003/sig000000ae )
  );
  MUXCY   \blk00000003/blk00000088  (
    .CI(\blk00000003/sig000000ab ),
    .DI(\blk00000003/sig0000009d ),
    .S(\blk00000003/sig000000ac ),
    .O(\blk00000003/sig000000a8 )
  );
  MUXCY   \blk00000003/blk00000087  (
    .CI(\blk00000003/sig000000a8 ),
    .DI(\blk00000003/sig0000009e ),
    .S(\blk00000003/sig000000a9 ),
    .O(\blk00000003/sig000000a5 )
  );
  MUXCY   \blk00000003/blk00000086  (
    .CI(\blk00000003/sig000000a5 ),
    .DI(\blk00000003/sig0000009f ),
    .S(\blk00000003/sig000000a6 ),
    .O(\blk00000003/sig000000a2 )
  );
  XORCY   \blk00000003/blk00000085  (
    .CI(\blk00000003/sig000000ab ),
    .LI(\blk00000003/sig000000ac ),
    .O(\blk00000003/sig000000ad )
  );
  XORCY   \blk00000003/blk00000084  (
    .CI(\blk00000003/sig000000a8 ),
    .LI(\blk00000003/sig000000a9 ),
    .O(\blk00000003/sig000000aa )
  );
  XORCY   \blk00000003/blk00000083  (
    .CI(\blk00000003/sig000000a5 ),
    .LI(\blk00000003/sig000000a6 ),
    .O(\blk00000003/sig000000a7 )
  );
  XORCY   \blk00000003/blk00000082  (
    .CI(\blk00000003/sig000000a2 ),
    .LI(\blk00000003/sig000000a3 ),
    .O(\blk00000003/sig000000a4 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000081  (
    .C(clk),
    .D(\blk00000003/sig0000009a ),
    .Q(\blk00000003/sig000000a1 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000080  (
    .C(clk),
    .D(\blk00000003/sig00000090 ),
    .Q(\blk00000003/sig00000057 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000007f  (
    .C(clk),
    .D(\blk00000003/sig00000093 ),
    .Q(\blk00000003/sig000000a0 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000007e  (
    .C(clk),
    .D(\blk00000003/sig00000096 ),
    .Q(\blk00000003/sig0000009f )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000007d  (
    .C(clk),
    .D(\blk00000003/sig00000099 ),
    .Q(\blk00000003/sig0000009e )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000007c  (
    .C(clk),
    .D(\blk00000003/sig0000009c ),
    .Q(\blk00000003/sig0000009d )
  );
  MUXCY   \blk00000003/blk0000007b  (
    .CI(NlwRenamedSig_OI_rfd),
    .DI(\blk00000003/sig0000008e ),
    .S(\blk00000003/sig0000009b ),
    .O(\blk00000003/sig00000097 )
  );
  XORCY   \blk00000003/blk0000007a  (
    .CI(NlwRenamedSig_OI_rfd),
    .LI(\blk00000003/sig0000009b ),
    .O(\blk00000003/sig0000009c )
  );
  MUXCY   \blk00000003/blk00000079  (
    .CI(\blk00000003/sig0000008f ),
    .DI(\blk00000003/sig0000001a ),
    .S(NlwRenamedSig_OI_rfd),
    .O(\blk00000003/sig0000009a )
  );
  MUXCY   \blk00000003/blk00000078  (
    .CI(\blk00000003/sig00000097 ),
    .DI(\blk00000003/sig0000001a ),
    .S(\blk00000003/sig00000098 ),
    .O(\blk00000003/sig00000094 )
  );
  MUXCY   \blk00000003/blk00000077  (
    .CI(\blk00000003/sig00000094 ),
    .DI(\blk00000003/sig0000001a ),
    .S(\blk00000003/sig00000095 ),
    .O(\blk00000003/sig00000091 )
  );
  MUXCY   \blk00000003/blk00000076  (
    .CI(\blk00000003/sig00000091 ),
    .DI(\blk00000003/sig0000001a ),
    .S(\blk00000003/sig00000092 ),
    .O(\blk00000003/sig0000008f )
  );
  XORCY   \blk00000003/blk00000075  (
    .CI(\blk00000003/sig00000097 ),
    .LI(\blk00000003/sig00000098 ),
    .O(\blk00000003/sig00000099 )
  );
  XORCY   \blk00000003/blk00000074  (
    .CI(\blk00000003/sig00000094 ),
    .LI(\blk00000003/sig00000095 ),
    .O(\blk00000003/sig00000096 )
  );
  XORCY   \blk00000003/blk00000073  (
    .CI(\blk00000003/sig00000091 ),
    .LI(\blk00000003/sig00000092 ),
    .O(\blk00000003/sig00000093 )
  );
  XORCY   \blk00000003/blk00000072  (
    .CI(\blk00000003/sig0000008f ),
    .LI(NlwRenamedSig_OI_rfd),
    .O(\blk00000003/sig00000090 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000071  (
    .C(clk),
    .D(dividend_0[0]),
    .Q(\blk00000003/sig00000058 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000070  (
    .C(clk),
    .D(dividend_0[1]),
    .Q(\blk00000003/sig00000059 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000006f  (
    .C(clk),
    .D(dividend_0[2]),
    .Q(\blk00000003/sig0000005a )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000006e  (
    .C(clk),
    .D(dividend_0[3]),
    .Q(\blk00000003/sig0000005b )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000006d  (
    .C(clk),
    .D(dividend_0[4]),
    .Q(\blk00000003/sig0000005c )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000006c  (
    .C(clk),
    .D(dividend_0[5]),
    .Q(\blk00000003/sig0000005d )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000006b  (
    .C(clk),
    .D(dividend_0[6]),
    .Q(\blk00000003/sig0000005e )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000006a  (
    .C(clk),
    .D(dividend_0[7]),
    .Q(\blk00000003/sig0000008e )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000069  (
    .C(clk),
    .D(\blk00000003/sig0000008d ),
    .Q(quotient_2[0])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000068  (
    .C(clk),
    .D(\blk00000003/sig0000008c ),
    .Q(quotient_2[1])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000067  (
    .C(clk),
    .D(\blk00000003/sig0000008b ),
    .Q(quotient_2[2])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000066  (
    .C(clk),
    .D(\blk00000003/sig0000008a ),
    .Q(quotient_2[3])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000065  (
    .C(clk),
    .D(\blk00000003/sig00000089 ),
    .Q(quotient_2[4])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000064  (
    .C(clk),
    .D(\blk00000003/sig00000088 ),
    .Q(quotient_2[5])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000063  (
    .C(clk),
    .D(\blk00000003/sig00000087 ),
    .Q(quotient_2[6])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000062  (
    .C(clk),
    .D(\blk00000003/sig00000086 ),
    .Q(quotient_2[7])
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000061  (
    .C(clk),
    .D(\blk00000003/sig00000081 ),
    .Q(\blk00000003/sig00000085 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000060  (
    .C(clk),
    .D(\blk00000003/sig00000080 ),
    .Q(\blk00000003/sig00000084 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000005f  (
    .C(clk),
    .D(\blk00000003/sig0000007f ),
    .Q(\blk00000003/sig00000083 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000005e  (
    .C(clk),
    .D(\blk00000003/sig0000007e ),
    .Q(\blk00000003/sig00000082 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000005d  (
    .C(clk),
    .D(\blk00000003/sig0000007d ),
    .Q(\blk00000003/sig00000081 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000005c  (
    .C(clk),
    .D(\blk00000003/sig0000007c ),
    .Q(\blk00000003/sig00000080 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000005b  (
    .C(clk),
    .D(\blk00000003/sig0000007b ),
    .Q(\blk00000003/sig0000007f )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000005a  (
    .C(clk),
    .D(\blk00000003/sig0000007a ),
    .Q(\blk00000003/sig0000007e )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000059  (
    .C(clk),
    .D(\blk00000003/sig00000079 ),
    .Q(\blk00000003/sig0000007d )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000058  (
    .C(clk),
    .D(\blk00000003/sig00000078 ),
    .Q(\blk00000003/sig0000007c )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000057  (
    .C(clk),
    .D(\blk00000003/sig00000077 ),
    .Q(\blk00000003/sig0000007b )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000056  (
    .C(clk),
    .D(\blk00000003/sig00000076 ),
    .Q(\blk00000003/sig0000007a )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000055  (
    .C(clk),
    .D(\blk00000003/sig00000075 ),
    .Q(\blk00000003/sig00000079 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000054  (
    .C(clk),
    .D(\blk00000003/sig00000074 ),
    .Q(\blk00000003/sig00000078 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000053  (
    .C(clk),
    .D(\blk00000003/sig00000073 ),
    .Q(\blk00000003/sig00000077 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000052  (
    .C(clk),
    .D(\blk00000003/sig00000072 ),
    .Q(\blk00000003/sig00000076 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000051  (
    .C(clk),
    .D(\blk00000003/sig00000071 ),
    .Q(\blk00000003/sig00000075 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000050  (
    .C(clk),
    .D(\blk00000003/sig00000070 ),
    .Q(\blk00000003/sig00000074 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000004f  (
    .C(clk),
    .D(\blk00000003/sig0000006f ),
    .Q(\blk00000003/sig00000073 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000004e  (
    .C(clk),
    .D(\blk00000003/sig0000006e ),
    .Q(\blk00000003/sig00000072 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000004d  (
    .C(clk),
    .D(\blk00000003/sig0000006d ),
    .Q(\blk00000003/sig00000071 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000004c  (
    .C(clk),
    .D(\blk00000003/sig0000006c ),
    .Q(\blk00000003/sig00000070 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000004b  (
    .C(clk),
    .D(\blk00000003/sig0000006b ),
    .Q(\blk00000003/sig0000006f )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000004a  (
    .C(clk),
    .D(\blk00000003/sig0000006a ),
    .Q(\blk00000003/sig0000006e )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000049  (
    .C(clk),
    .D(\blk00000003/sig00000069 ),
    .Q(\blk00000003/sig0000006d )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000048  (
    .C(clk),
    .D(\blk00000003/sig00000068 ),
    .Q(\blk00000003/sig0000006c )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000047  (
    .C(clk),
    .D(\blk00000003/sig00000067 ),
    .Q(\blk00000003/sig0000006b )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000046  (
    .C(clk),
    .D(\blk00000003/sig00000066 ),
    .Q(\blk00000003/sig0000006a )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000045  (
    .C(clk),
    .D(\blk00000003/sig00000065 ),
    .Q(\blk00000003/sig00000069 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000044  (
    .C(clk),
    .D(\blk00000003/sig00000064 ),
    .Q(\blk00000003/sig00000068 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000043  (
    .C(clk),
    .D(\blk00000003/sig00000063 ),
    .Q(\blk00000003/sig00000067 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000042  (
    .C(clk),
    .D(\blk00000003/sig00000062 ),
    .Q(\blk00000003/sig00000066 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000041  (
    .C(clk),
    .D(divisor_1[3]),
    .Q(\blk00000003/sig00000065 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000040  (
    .C(clk),
    .D(divisor_1[2]),
    .Q(\blk00000003/sig00000064 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000003f  (
    .C(clk),
    .D(divisor_1[1]),
    .Q(\blk00000003/sig00000063 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000003e  (
    .C(clk),
    .D(divisor_1[0]),
    .Q(\blk00000003/sig00000062 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000003d  (
    .C(clk),
    .D(\blk00000003/sig00000031 ),
    .Q(\blk00000003/sig00000061 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000003c  (
    .C(clk),
    .D(\blk00000003/sig0000003f ),
    .Q(\blk00000003/sig0000002f )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000003b  (
    .C(clk),
    .D(\blk00000003/sig0000003d ),
    .Q(\blk00000003/sig0000002e )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000003a  (
    .C(clk),
    .D(\blk00000003/sig0000003b ),
    .Q(\blk00000003/sig0000002d )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000039  (
    .C(clk),
    .D(\blk00000003/sig00000039 ),
    .Q(\blk00000003/sig0000002c )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000038  (
    .C(clk),
    .D(\blk00000003/sig00000060 ),
    .Q(\blk00000003/sig0000002b )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000037  (
    .C(clk),
    .D(\blk00000003/sig0000005e ),
    .Q(\blk00000003/sig0000005f )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000036  (
    .C(clk),
    .D(\blk00000003/sig0000005d ),
    .Q(\blk00000003/sig00000055 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000035  (
    .C(clk),
    .D(\blk00000003/sig0000005c ),
    .Q(\blk00000003/sig00000054 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000034  (
    .C(clk),
    .D(\blk00000003/sig0000005b ),
    .Q(\blk00000003/sig00000053 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000033  (
    .C(clk),
    .D(\blk00000003/sig0000005a ),
    .Q(\blk00000003/sig00000052 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000032  (
    .C(clk),
    .D(\blk00000003/sig00000059 ),
    .Q(\blk00000003/sig00000051 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000031  (
    .C(clk),
    .D(\blk00000003/sig00000058 ),
    .Q(\blk00000003/sig00000050 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000030  (
    .C(clk),
    .D(\blk00000003/sig00000057 ),
    .Q(\blk00000003/sig0000004f )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000002f  (
    .C(clk),
    .D(\blk00000003/sig00000055 ),
    .Q(\blk00000003/sig00000056 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000002e  (
    .C(clk),
    .D(\blk00000003/sig00000054 ),
    .Q(\blk00000003/sig0000004c )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000002d  (
    .C(clk),
    .D(\blk00000003/sig00000053 ),
    .Q(\blk00000003/sig0000004b )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000002c  (
    .C(clk),
    .D(\blk00000003/sig00000052 ),
    .Q(\blk00000003/sig0000004a )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000002b  (
    .C(clk),
    .D(\blk00000003/sig00000051 ),
    .Q(\blk00000003/sig00000049 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000002a  (
    .C(clk),
    .D(\blk00000003/sig00000050 ),
    .Q(\blk00000003/sig00000048 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000029  (
    .C(clk),
    .D(\blk00000003/sig0000004f ),
    .Q(\blk00000003/sig00000047 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000028  (
    .C(clk),
    .D(\blk00000003/sig0000004e ),
    .Q(\blk00000003/sig00000046 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000027  (
    .C(clk),
    .D(\blk00000003/sig0000004c ),
    .Q(\blk00000003/sig0000004d )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000026  (
    .C(clk),
    .D(\blk00000003/sig0000004b ),
    .Q(\blk00000003/sig00000043 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000025  (
    .C(clk),
    .D(\blk00000003/sig0000004a ),
    .Q(\blk00000003/sig00000042 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000024  (
    .C(clk),
    .D(\blk00000003/sig00000049 ),
    .Q(\blk00000003/sig00000041 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000023  (
    .C(clk),
    .D(\blk00000003/sig00000048 ),
    .Q(\blk00000003/sig00000040 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000022  (
    .C(clk),
    .D(\blk00000003/sig00000047 ),
    .Q(\blk00000003/sig0000003e )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000021  (
    .C(clk),
    .D(\blk00000003/sig00000046 ),
    .Q(\blk00000003/sig0000003c )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000020  (
    .C(clk),
    .D(\blk00000003/sig00000045 ),
    .Q(\blk00000003/sig0000003a )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000001f  (
    .C(clk),
    .D(\blk00000003/sig00000043 ),
    .Q(\blk00000003/sig00000044 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000001e  (
    .C(clk),
    .D(\blk00000003/sig00000042 ),
    .Q(\blk00000003/sig00000036 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000001d  (
    .C(clk),
    .D(\blk00000003/sig00000041 ),
    .Q(\blk00000003/sig00000035 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk0000001c  (
    .C(clk),
    .D(\blk00000003/sig00000040 ),
    .Q(\blk00000003/sig00000034 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000001b  (
    .C(clk),
    .D(\blk00000003/sig0000003e ),
    .Q(\blk00000003/sig0000003f )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000001a  (
    .C(clk),
    .D(\blk00000003/sig0000003c ),
    .Q(\blk00000003/sig0000003d )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000019  (
    .C(clk),
    .D(\blk00000003/sig0000003a ),
    .Q(\blk00000003/sig0000003b )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000018  (
    .C(clk),
    .D(\blk00000003/sig00000038 ),
    .Q(\blk00000003/sig00000039 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000017  (
    .C(clk),
    .D(\blk00000003/sig00000036 ),
    .Q(\blk00000003/sig00000037 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000016  (
    .C(clk),
    .D(\blk00000003/sig00000035 ),
    .Q(\blk00000003/sig00000032 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000015  (
    .C(clk),
    .D(\blk00000003/sig00000034 ),
    .Q(\blk00000003/sig00000030 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000014  (
    .C(clk),
    .D(\blk00000003/sig00000032 ),
    .Q(\blk00000003/sig00000033 )
  );
  FD #(
    .INIT ( 1'b0 ))
  \blk00000003/blk00000013  (
    .C(clk),
    .D(\blk00000003/sig00000030 ),
    .Q(\blk00000003/sig00000031 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000012  (
    .C(clk),
    .D(\blk00000003/sig0000002f ),
    .Q(\blk00000003/sig00000028 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000011  (
    .C(clk),
    .D(\blk00000003/sig0000002e ),
    .Q(\blk00000003/sig00000026 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000010  (
    .C(clk),
    .D(\blk00000003/sig0000002d ),
    .Q(\blk00000003/sig00000024 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000000f  (
    .C(clk),
    .D(\blk00000003/sig0000002c ),
    .Q(\blk00000003/sig00000022 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000000e  (
    .C(clk),
    .D(\blk00000003/sig0000002b ),
    .Q(\blk00000003/sig00000020 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000000d  (
    .C(clk),
    .D(\blk00000003/sig0000002a ),
    .Q(\blk00000003/sig0000001e )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000000c  (
    .C(clk),
    .D(\blk00000003/sig00000028 ),
    .Q(\blk00000003/sig00000029 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000000b  (
    .C(clk),
    .D(\blk00000003/sig00000026 ),
    .Q(\blk00000003/sig00000027 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk0000000a  (
    .C(clk),
    .D(\blk00000003/sig00000024 ),
    .Q(\blk00000003/sig00000025 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000009  (
    .C(clk),
    .D(\blk00000003/sig00000022 ),
    .Q(\blk00000003/sig00000023 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000008  (
    .C(clk),
    .D(\blk00000003/sig00000020 ),
    .Q(\blk00000003/sig00000021 )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000007  (
    .C(clk),
    .D(\blk00000003/sig0000001e ),
    .Q(\blk00000003/sig0000001f )
  );
  FD #(
    .INIT ( 1'b1 ))
  \blk00000003/blk00000006  (
    .C(clk),
    .D(\blk00000003/sig0000001c ),
    .Q(\blk00000003/sig0000001d )
  );
  VCC   \blk00000003/blk00000005  (
    .P(NlwRenamedSig_OI_rfd)
  );
  GND   \blk00000003/blk00000004  (
    .G(\blk00000003/sig0000001a )
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
