The following files were generated for 'DIV10' in directory
/home/shawn/projects/ise-experiments/tempmonitor/ipcore_dir/

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * DIV10.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * DIV10.ngc
   * DIV10.v
   * DIV10.veo

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * DIV10.veo

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * DIV10.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * DIV10.sym

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * DIV10_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * DIV10.gise
   * DIV10.xise

Deliver Readme:
   Readme file for the IP.

   * DIV10_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * DIV10_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

