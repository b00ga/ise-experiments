`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    11:40:21 04/20/2015 
// Design Name: 
// Module Name:    STOPLIGHT 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description:  Determine which light (green/yellow/red) to turn on based
//		on current temperature thresholds
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module STOPLIGHT(
		output reg[2:0] stoplight = 1'b100,
		input  wire [3:0] hundreds,
		input  wire [3:0] tens,
		input  wire [3:0] ones,
		input	 wire [7:0] warn,
		input  wire [7:0] alarm,
		input wire clock
    );

	reg [7:0] num = 8'b0;

	always @(posedge clock)
	begin
		num <= hundreds*100+tens*10+ones;
		if ( num < warn )
			stoplight <= 3'b100;
		else if ( num >= alarm )
			stoplight <= 3'b001;
		else
			stoplight <= 3'b010;
	end

endmodule
