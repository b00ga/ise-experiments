`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    20:22:31 04/12/2015 
// Design Name: 
// Module Name:    LOOKUP 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description:  ADC Vref calibrated to give us 1 degree F/step
//		This module maps those known ADC inputs to the known calibrated temps
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module LOOKUP(
		output reg [3:0] hundreds,
		output reg [3:0] tens,
		output reg [3:0] ones,
		input wire [7:0] num
    );

	always @(num)
	begin
		case (num)
			8'd110: begin
				hundreds	<= 4'd0;
				tens		<= 4'd5;
				ones		<= 4'd4;
			end
			8'd111: begin
				hundreds	<= 4'd0;
				tens		<= 4'd5;
				ones		<= 4'd5;
			end
			8'd112: begin
				hundreds	<= 4'd0;
				tens		<= 4'd5;
				ones		<= 4'd6;
			end
			8'd113: begin
				hundreds	<= 4'd0;
				tens		<= 4'd5;
				ones		<= 4'd7;
			end
			8'd114: begin
				hundreds	<= 4'd0;
				tens		<= 4'd5;
				ones		<= 4'd8;
			end
			8'd115: begin
				hundreds	<= 4'd0;
				tens		<= 4'd5;
				ones		<= 4'd9;
			end
			8'd116: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd0;
			end
			8'd117: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd1;
			end
			8'd118: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd2;
			end
			8'd119: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd3;
			end
			8'd120: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd4;
			end
			8'd121: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd5;
			end
			8'd122: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd6;
			end
			8'd123: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd7;
			end
			8'd124: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd8;
			end
			8'd125: begin
				hundreds	<= 4'd0;
				tens		<= 4'd6;
				ones		<= 4'd9;
			end
			8'd126: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd0;
			end
			8'd127: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd1;
			end
			8'd128: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd2;
			end
			8'd129: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd3;
			end
			8'd130: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd4;
			end
			8'd131: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd5;
			end
			8'd132: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd6;
			end
			8'd133: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd7;
			end
			8'd134: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd8;
			end
			8'd135: begin
				hundreds	<= 4'd0;
				tens		<= 4'd7;
				ones		<= 4'd9;
			end
			8'd136: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd0;
			end
			8'd137: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd1;
			end
			8'd138: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd2;
			end
			8'd139: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd3;
			end
			8'd140: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd4;
			end
			8'd141: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd5;
			end
			8'd142: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd6;
			end
			8'd143: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd7;
			end
			8'd144: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd8;
			end
			8'd145: begin
				hundreds	<= 4'd0;
				tens		<= 4'd8;
				ones		<= 4'd9;
			end
			8'd146: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd0;
			end
			8'd147: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd1;
			end
			8'd148: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd2;
			end
			8'd149: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd3;
			end
			8'd150: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd4;
			end
			8'd151: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd5;
			end
			8'd152: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd6;
			end
			8'd153: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd7;
			end
			8'd154: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd8;
			end
			8'd155: begin
				hundreds	<= 4'd0;
				tens		<= 4'd9;
				ones		<= 4'd9;
			end
			8'd156: begin
				hundreds	<= 4'd1;
				tens		<= 4'd0;
				ones		<= 4'd0;
			end
			8'd157: begin
				hundreds	<= 4'd1;
				tens		<= 4'd0;
				ones		<= 4'd1;
			end
			8'd158: begin
				hundreds	<= 4'd1;
				tens		<= 4'd0;
				ones		<= 4'd2;
			end
			8'd159: begin
				hundreds	<= 4'd1;
				tens		<= 4'd0;
				ones		<= 4'd3;
			end
			8'd160: begin
				hundreds	<= 4'd1;
				tens		<= 4'd0;
				ones		<= 4'd4;
			end
		endcase
	end

endmodule
