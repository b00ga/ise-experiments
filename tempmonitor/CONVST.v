`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Merrimack College
// Engineer: Shawn K. O'Shea
// 
// Create Date:    18:24:42 04/11/2015 
// Design Name: 
// Module Name:    CONVST 
// Project Name: Temperature Monitor
// Target Devices: 
// Tool versions: 
// Description:  On enable pulse, send a conversion start to the ADC
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CONVST(
    output reg convst = 0,
    input wire EN,
    input wire CLK
    );
	 
	always @(posedge CLK)
	begin
		if (EN)
			convst <= 1;
		else
			convst <=0;
	end
	
endmodule
