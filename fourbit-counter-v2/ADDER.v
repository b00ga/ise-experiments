`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:26:18 02/16/2015 
// Design Name: 
// Module Name:    ADDER 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ADDER(
                output reg [3:0] sum = 4'b0,
					 input wire en,
                input wire clock
    );

	always @(posedge clock)
	begin
		if (en)
			sum <= sum + 1;
	end
endmodule

