`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:29:44 02/18/2015
// Design Name:   TOP
// Module Name:   /home/shawn/projects/ise-experiments/fourbit-counter-v2/TOP_TF.v
// Project Name:  fourbit-counter-v2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: TOP
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TOP_TF;

	// Inputs
	reg clock;

	// Outputs
	wire [3:0] count;
	wire secclock;

	// Instantiate the Unit Under Test (UUT)
	TOP uut (
		.count(count), 
		.secclock(secclock), 
		.clock(clock)
	);
	
	initial
			$monitor($time, " Count = %b", count);

	initial begin
		// Initialize Inputs
		clock = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		forever #20
      begin
			clock = ~clock;
      end

	end
      
endmodule

