`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:59:28 02/15/2015 
// Design Name: 
// Module Name:    COUNTDOWN 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module COUNTDOWN(
	output reg [8:0] minutes = 8'd2,
	output reg [7:0] seconds = 7'd15,
	output reg stop,
	input wire run,
	input wire clock
    );

	always @(posedge clock)
	begin
		if ( run )
		begin
			if ( (minutes == 0) && (seconds == 0) )
				stop = 1'b1;
			else
			begin
				if (seconds == 0)
				begin
					minutes <= minutes - 1;
					seconds <= 59;
				end
				else
					seconds <= seconds - 1;
			end
		end
	end
endmodule
