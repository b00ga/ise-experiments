`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:14:01 02/15/2015
// Design Name:   TOP
// Module Name:   /home/shawn/projects/ise-experiments/kitchen-timer/TOP_TF.v
// Project Name:  kitchen-timer
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: TOP
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TOP_TF;

	// Inputs
	reg run;
	reg clock;

	// Outputs
	wire [8:0] minutes;
	wire [7:0] seconds;

	// Instantiate the Unit Under Test (UUT)
	TOP uut (
		.minutes(minutes),
		.seconds(seconds),
		.run(run),
		.clock(clock)
	);
	
	initial
		$monitor($time, " Time is %02d:%02d", minutes, seconds);


	initial begin
		// Initialize Inputs
		clock = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		forever #20
		begin
			clock = ~clock;
		end
		
	end
	
	initial begin
		run = 0;
		#120;
		run = 1;
		#200;
		run = 0;
		#40;
		run = 1;
		#300;
		run = 0;
		#300;
		run = 1;
	end
	     
endmodule

