`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:08:06 02/15/2015 
// Design Name: 
// Module Name:    TOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TOP(
	output wire [8:0] minutes,
	output wire [7:0] seconds,
	input wire run,
   input wire clock
	);

	wire state;
	wire statereset;
	RUNSTATE rs(state,run,statereset,clock);
	COUNTDOWN c1(minutes,seconds,statereset,state,clock);

endmodule
