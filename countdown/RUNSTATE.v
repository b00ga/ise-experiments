`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:41:36 02/15/2015 
// Design Name: 
// Module Name:    RUNSTATE 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module RUNSTATE(
	output reg newstate = 1'b0,
	input wire state,
	input wire stop,
	input wire clock
    );
	 
	always @(posedge clock)
	begin
		if ( stop )
			newstate = 1'b0;
		else
			newstate = state;
	end
endmodule
