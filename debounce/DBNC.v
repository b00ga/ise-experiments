`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:22:43 02/25/2015 
// Design Name: 
// Module Name:    DBNC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DBNC(
		output wire OUTPUT,
		input wire INPUT,
		input wire CLOCK
    );

INPUT in(OUTPUT,INPUT,CLOCK);

endmodule
