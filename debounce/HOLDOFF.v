`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:10:52 02/25/2015 
// Design Name: 
// Module Name:    HOLDOFF 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module HOLDOFF(
		output reg holdoff = 1'b0,
		input wire enable,
		input wire clock
    );

	reg [25:0] timer = 26'b0;
	always @(posedge clock)
	begin
		if (enable)
		begin
			timer <= 26'b0;
			holdoff <= 1'b1;
		end
		else if (timer < 26'd12499999)
		begin
			timer <= timer + 1;
			holdoff <= 1'b1;
		end
		else
			holdoff <= 1'b0;
	end

endmodule
