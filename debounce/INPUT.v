`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:18:37 02/25/2015 
// Design Name: 
// Module Name:    INPUT 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module INPUT(
		output reg OUTPUT = 1'b0,
		input wire INPUT,
		input wire CLOCK
    );

wire HO;
reg COUNTING = 1'b0;
HOLDOFF ho(HO,COUNTING,CLOCK);

always @(posedge CLOCK)
begin

if (INPUT && !HO) // button pushed, HO not running
begin
	COUNTING <= 1'b1;
	OUTPUT <= 1'b1;
end
else
	OUTPUT <= 1'b0;

end

endmodule
