`timescale 10ns / 1ns

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:51:52 02/25/2015
// Design Name:   DBNC
// Module Name:   /home/shawn/projects/ise-experiments/debounce/DBNC_TF.v
// Project Name:  debounce
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: DBNC
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module DBNC_TF;

	// Inputs
	reg INPUT;
	reg CLOCK;

	// Outputs
	wire OUTPUT;

	// Instantiate the Unit Under Test (UUT)
	DBNC uut (
		.OUTPUT(OUTPUT), 
		.INPUT(INPUT), 
		.CLOCK(CLOCK)
	);
	
	initial
		$monitor($time, " Output changed to %02d", OUTPUT);


	initial begin
		// Initialize Inputs
		INPUT = 0;
		CLOCK = 0;

		// Wait 100 ns for global reset to finish
		#10;
        
		// Add stimulus here
		forever #2
			CLOCK = ~CLOCK;
	end
   
	initial begin
		INPUT=1;
		#2
		INPUT=0;
		#2
		INPUT=1;
		#2
		INPUT=0;
	end
   
endmodule

