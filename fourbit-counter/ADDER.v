`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:13:58 02/07/2015 
// Design Name: 
// Module Name:    ADDER 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ADDER(
		output reg [3:0] sum = 4'b0,
		input wire [25:0] timer, 
		input wire clock
    );

	always @(posedge clock)
	begin
		if (timer == 26'd49999999)
			sum <= sum + 1;
	end
endmodule
