`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:55:04 02/06/2015 
// Design Name: 
// Module Name:    TOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TOP( 
		output wire [3:0] count,
		output reg [3:0] digitselect,
		output wire [7:0] segmentselect,
		input wire clock
    );
	
wire [25:0] bus;
TIMER t1(bus,clock);
ADDER a1(count,bus,clock);
SEGENCODER se(segmentselect,count,clock);

// digitselect - RMD X X LMD
// 0 - ON / 1 - OFF
always @(posedge clock)
begin
	digitselect <= 4'b1110;
end

endmodule
