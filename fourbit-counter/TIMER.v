`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:08:44 02/07/2015 
// Design Name: 
// Module Name:    TIMER 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TIMER(
		output reg [25:0] waittime = 26'b0,
		input wire clock
    );
	 
	always @(posedge clock)
	begin
		if ( waittime < 26'd50000000 )
			waittime <= waittime + 1;
		else
			waittime <= 26'b0;
	end

endmodule
