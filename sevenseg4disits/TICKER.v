`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:24:34 02/22/2015 
// Design Name: 
// Module Name:    TICKER 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TICKER(
        output reg tick = 1'b0,
        input wire clock
    );

        reg [17:0] ticker = 18'd0;

        always @(posedge clock)
        begin
                //if (ticker == 18'd199999)
					 if (ticker == 18'd399999)
                begin
                        ticker <= 0;
                        tick <= 1'b1;
                end
                else if (ticker == 18'd0)
                begin
                        ticker <= 18'd1;
                        tick <= 1'b0;
                end
                else
                        ticker <= ticker + 1;
        end

endmodule

