`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:19:46 02/25/2015
// Design Name:   TOP
// Module Name:   /home/shawn/projects/ise-experiments/sevenseg4disits/TOP_TF.v
// Project Name:  sevenseg4disits
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: TOP
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TOP_TF;

	// Inputs
	reg clock;

	// Outputs
	wire [3:0] digitselect;
	wire [7:0] segmentselect;

	// Instantiate the Unit Under Test (UUT)
	TOP uut (
		.digitselect(digitselect), 
		.segmentselect(segmentselect), 
		.clock(clock)
	);

	initial	$monitor($time, " 7seg code = %b", segmentselect);

	initial begin
		// Initialize Inputs
		clock = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here                
		forever #20
      begin
			clock = ~clock;
      end
	end
      
endmodule

