`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:23:45 02/22/2015 
// Design Name: 
// Module Name:    TOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TOP(
		output wire [3:0] digitselect,
		output wire [7:0] segmentselect,
		input wire clock
    );

	wire pulse;
	wire [1:0] count;
	TICKER t1(pulse,clock);
	COUNTER c1(count,pulse,clock);
	SELECTENCODER e1(digitselect,segmentselect,count);

endmodule
