`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:25:25 02/22/2015 
// Design Name: 
// Module Name:    ENCODER 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SELECTENCODER(
		output reg [3:0] digitselect,
		output reg [7:0] segmentselect,
		input wire [1:0] num
    );

	always @(num)
	begin
		case (num)
			2'b00 : begin digitselect <= 4'b1110 ; segmentselect <= 8'b11000000; end
			2'b01 : begin digitselect <= 4'b1101 ; segmentselect <= 8'b11111001; end
			2'b10 : begin digitselect <= 4'b1011 ; segmentselect <= 8'b10100100; end
			2'b11 : begin digitselect <= 4'b0111 ; segmentselect <= 8'b10110000; end
		endcase
	end

endmodule
