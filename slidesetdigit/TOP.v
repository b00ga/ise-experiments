`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:26:53 02/22/2015 
// Design Name: 
// Module Name:    TOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TOP(
        output reg [3:0] digitselect,
        output reg [7:0] segmentselect,
		  input wire[3:0] num,
		  input wire [3:0] buttons
    );
	 
always @(buttons)
begin
	digitselect <= ~buttons;
	case (num)
		4'b0000 : segmentselect <= 8'b11000000 ;
		4'b0001 : segmentselect <= 8'b11111001 ;
		4'b0010 : segmentselect <= 8'b10100100 ;
		4'b0011 : segmentselect <= 8'b10110000 ;
		4'b0100 : segmentselect <= 8'b10011001 ;
		4'b0101 : segmentselect <= 8'b10010010 ;
		4'b0110 : segmentselect <= 8'b10000010 ;
		4'b0111 : segmentselect <= 8'b11111000 ;
		4'b1000 : segmentselect <= 8'b10000000 ;
		4'b1001 : segmentselect <= 8'b10010000 ;
		4'b1010 : segmentselect <= 8'b00001000 ;
		4'b1011 : segmentselect <= 8'b00000011 ;
		4'b1100 : segmentselect <= 8'b01000110 ;
		4'b1101 : segmentselect <= 8'b00100001 ;
		4'b1110 : segmentselect <= 8'b00000110 ;
		4'b1111 : segmentselect <= 8'b00001110 ;
		default : segmentselect <= 8'b11111111 ;
	endcase
end

endmodule
