`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:59:41 03/13/2015 
// Design Name: 
// Module Name:    DBNC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DBNC ( 
			output reg OUT,
			input wire ASYNC,
			input wire CLOCK
    );

	reg COUNTING = 1'b0;
	reg [25:0] COUNT = 26'b0;
	reg SYNC;
	
	always @( posedge CLOCK ) // Syncronize the button press to the clock
		SYNC <= ASYNC;

	always @( posedge CLOCK )
	begin
		if ( SYNC && ! COUNTING ) // Button pressed. Holdoff counter not started
			OUT <= 1'b1;
		else // Button not pressed
			OUT <= 1'b0;
	end
	
	always @( posedge CLOCK )
	begin
		if ( COUNTING )
			COUNT <= COUNT + 1;
		else 
			COUNT <= 26'b0;
	end
	
	always @ ( posedge CLOCK )
	begin
		if ( SYNC && ! COUNTING )
			COUNTING <= 1'b1;
		else if (COUNT ==  26'd12499999)
			COUNTING <= 1'b0;
	end

endmodule
