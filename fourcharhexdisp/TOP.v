`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:57:29 02/25/2015 
// Design Name: 
// Module Name:    TOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TOP(
		output wire [3:0] digitselect,
		output wire [7:0] segmentselect,
		input wire clock
    );

C4HEXDISP c4hex(segmentselect,digitselect,4'd4,4'd3,4'd2,4'd1,clock);

endmodule
