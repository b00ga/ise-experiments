`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:10:12 03/13/2015 
// Design Name: 
// Module Name:    TOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TOP(
			output wire [7:0] segmentselect,
			output wire [3:0] digitselect,
			input wire button,
			input wire clock
    );

	wire sync;
	wire [3:0] num;
	DBNC btn(sync,button,clock);
	ADDER a1(num,sync,clock);
	C4HEXDISP d1(segmentselect,digitselect,4'b0,4'b0,4'b0,num,clock);

endmodule
