`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:54:38 02/16/2015 
// Design Name: 
// Module Name:    SEGENCODER 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SEGENCODER(
		output reg [7:0] sevenseg,
		input wire [3:0] num
    );

// sevenseg seg6 seg5 ... seg0
// 0 - ON / 1 - OFF
// ( diagram from Digilent demo code ) 
//      0
//     ---  
//  5 |   | 1
//     ---   <- 6
//  4 |   | 2
//     ---
//      3

always @(num)
begin
	case (num)
		4'b0000 : sevenseg <= 8'b11000000 ;
		4'b0001 : sevenseg <= 8'b11111001 ;
		4'b0010 : sevenseg <= 8'b10100100 ;
		4'b0011 : sevenseg <= 8'b10110000 ;
		4'b0100 : sevenseg <= 8'b10011001 ;
		4'b0101 : sevenseg <= 8'b10010010 ;
		4'b0110 : sevenseg <= 8'b10000010 ;
		4'b0111 : sevenseg <= 8'b11111000 ;
		4'b1000 : sevenseg <= 8'b10000000 ;
		4'b1001 : sevenseg <= 8'b10010000 ;
		4'b1010 : sevenseg <= 8'b00001000 ;
		4'b1011 : sevenseg <= 8'b00000011 ;
		4'b1100 : sevenseg <= 8'b01000110 ;
		4'b1101 : sevenseg <= 8'b00100001 ;
		4'b1110 : sevenseg <= 8'b00000110 ;
		4'b1111 : sevenseg <= 8'b00001110 ;
		default : sevenseg <= 8'b11111111 ;
	endcase
end

endmodule
