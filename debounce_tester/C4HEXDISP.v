`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:58:31 02/25/2015 
// Design Name: 
// Module Name:    C4HEXDISP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module C4HEXDISP(
		output wire [7:0] SEG_N,
		output wire [3:0] EN_N,
		input wire [3:0] C3,
		input wire [3:0] C2,
		input wire [3:0] C1,
		input wire [3:0] C0,
		input wire clock
    );

wire tick;
wire [1:0] count;
reg [3:0] currentdigit;

//TICKER t1(tick,clock);
//COUNTER c1(count, tick, clock);
SELECTENCODER en1(EN_N,2'b0);
SEGENCODER en2(SEG_N,currentdigit);

always @(EN_N)
begin
	case (2'b0)
		2'b00:	currentdigit <= C0;
		2'b01:	currentdigit <= C1;
		2'b10:	currentdigit <= C2;
		2'b11:	currentdigit <= C3;
	endcase
end

endmodule
